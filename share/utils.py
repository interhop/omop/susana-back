def string_cleaner(string_to_clean, to_erase):
    for ch in to_erase:
        if ch in string_to_clean:
            string_to_clean = string_to_clean.replace(ch, " ")
    return string_to_clean
