## FLASK-API ##
# To generate a new secret key:
# >>> import random, string
# >>> result = "".join([random.choice(string.printable) for _ in range(24)])
import os
from distutils.util import strtobool

from dotenv import load_dotenv
from requests.auth import HTTPBasicAuth

load_dotenv()

SECRET_KEY = os.environ.get("SECRET_KEY", "\nrIP\n#g!R?9\n6.-I])~yA:d+")

## Database ##
SQLALCHEMY_TRACK_MODIFICATIONS = False
PROPAGATE_EXCEPTIONS = True
JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ["access", "refresh"]
POSTGRES = {
    "user": os.environ.get("POSTGRES_USER", "user"),
    "password": os.environ.get("POSTGRES_PASSWORD", "password"),
    "database": os.environ.get("POSTGRES_DATABASE", "susana"),
    "host": os.environ.get("POSTGRES_HOST", "postgres"),
    "port": os.environ.get("POSTGRES_PORT", 5432),
}
SQLALCHEMY_DATABASE_URI = "postgresql://%(user)s:%(password)s@%(host)s:%(port)s/%(database)s" % POSTGRES

## SEND EMAIL ##
# recovery_key =
EMAIL_ADDRESS = os.environ.get("EMAIL_ADDRESS", "")
EMAIL_ADMIN_ADDRESS = os.environ.get("EMAIL_ADMIN_ADDRESS", "")
EMAIL_PASSWORD = os.environ.get("EMAIL_PASSWORD", "")
EMAIL_SMTP_HOST = os.environ.get("EMAIL_SMTP_HOST", "")
EMAIL_SMTP_PORT = os.environ.get("EMAIL_SMTP_PORT", "25")

# SOLR
SOLR_HOST = os.environ.get("SOLR_HOST", "solr")
SOLR_PORT = os.environ.get("SOLR_PORT", "8983")
SOLR_CORE = os.environ.get("SOLR_CORE", "susana_core")
SOLR_USER = os.environ.get("SOLR_USER", "user")
SOLR_PASSWORD = os.environ.get("SOLR_PASSWORD", "password")
SOLR_AUTH = HTTPBasicAuth(SOLR_USER, SOLR_PASSWORD)
SOLR_CSV_FILEPATH = "/app/tmp/solr.csv"

JOB_FREQUENCY_IN_SECONDS = os.environ.get("JOB_FREQUENCY_IN_SECONDS", 30)
INIT_SOLR_AT_STARTUP = bool(strtobool(os.environ.get("INIT_SOLR_AT_STARTUP", "false")))
