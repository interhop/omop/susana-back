from datetime import datetime, timedelta

from app.db import db


class StatisticModel(db.Model):
    __tablename__ = "mapper_statistic"
    __table_args__ = {"schema": "susana"}

    # m_concept_id references concept_id from concept table or m_concept_relationship_id from concept_relationship
    m_concept_id = db.Column(
        db.Integer, db.ForeignKey("susana.concept.concept_id"), primary_key=True
    )
    m_statistic_type_id = db.Column(db.Text(), primary_key=True)
    m_value_as_number = db.Column(db.Integer)
    m_value_as_string = db.Column(db.Text())
    m_value_as_concept_id = db.Column(db.Integer)
    m_value_as_datetime = db.Column(db.Date())
    m_qualifier_concept_id = db.Column(db.Integer)
    m_unit_concept_id = db.Column(db.Integer)
    m_algo_id = db.Column(db.Integer)
    m_user_id = db.Column(db.Integer)
    m_valid_start_datetime = db.Column(
        db.Date(), default=datetime.now(), primary_key=True
    )  # CURRENT_DATE
    m_valid_end_datetime = db.Column(
        db.Date(), default=datetime.now() + timedelta(weeks=5200)
    )
    m_invalid_reason = db.Column(db.Text())

    def __init__(
        self,
        m_concept_id,
        m_statistic_type_id,
        m_algo_id,
        m_user_id,
        m_value_as_number=None,
        m_value_as_string=None,
        m_value_as_concept_id=None,
        m_value_as_datetime=None,
        m_qualifier_concept_id=None,
        m_unit_concept_id=None,
        m_valid_end_datetime=None,
        m_invalid_reason=None,
    ):

        self.m_concept_id = m_concept_id
        self.m_statistic_type_id = m_statistic_type_id
        self.m_algo_id = m_algo_id
        self.m_user_id = m_user_id
        self.m_value_as_number = m_value_as_number
        self.m_value_as_string = m_value_as_string
        self.m_value_as_concept_id = m_value_as_concept_id
        self.m_value_as_datetime = m_value_as_datetime
        self.m_qualifier_concept_id = m_qualifier_concept_id
        self.m_unit_concept_id = m_unit_concept_id
        self.m_invalid_reason = m_invalid_reason
        if m_valid_end_datetime != None:
            self.m_valid_end_datetime = m_valid_end_datetime
        self.m_valid_start_datetime = datetime.now()

    def json(self):
        return {
            "m_concept_id": self.m_concept_id,
            "m_statistic_type_id": self.m_statistic_type_id,
            "m_value_as_number": self.m_value_as_number,
            "m_value_as_string": self.m_value_as_string,
            "m_value_as_concept_id": self.m_value_as_concept_id,
            "m_value_as_datetime": self.m_value_as_datetime,
        }

    @classmethod
    def find_by_id(cls, concept_id):
        return cls.query.filter_by(m_concept_id=concept_id).all()

    @classmethod
    def find_one_stat(cls, m_concept_id, m_statistic_type_id):
        return cls.query.filter_by(
            m_concept_id=m_concept_id,
            m_statistic_type_id=m_statistic_type_id,
            m_valid_end_datetime=None,
        ).first()

    @classmethod
    def find_all_users_with_one_stat(
        cls, m_concept_id, m_statistic_type_id, m_value_as_string, m_invalid_reason
    ):
        query_set = cls.query.filter_by(
            m_concept_id=m_concept_id,
            m_statistic_type_id=m_statistic_type_id,
            m_value_as_string=m_value_as_string,
            m_invalid_reason=m_invalid_reason,
        ).all()
        return [q.m_user_id for q in query_set]

    @classmethod
    def one_user_for_one_stat(
        cls, m_concept_id, m_statistic_type_id, m_user_id, m_invalid_reason
    ):
        return cls.query.filter_by(
            m_concept_id=m_concept_id,
            m_statistic_type_id=m_statistic_type_id,
            m_user_id=m_user_id,
            m_invalid_reason=m_invalid_reason,
        ).all()

    def upserting(self):
        db.session.add(self)
        db.session.commit()
