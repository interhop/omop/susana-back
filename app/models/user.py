from datetime import datetime, timedelta

from werkzeug.security import check_password_hash, generate_password_hash

from app.db import db


class UserModel(db.Model):
    __tablename__ = "mapper_user"
    __table_args__ = {"schema": "susana"}

    id = db.Column("m_user_id", db.Integer, primary_key=True, autoincrement=True)

    m_username = db.Column(db.String(255), nullable=False, unique=True)
    m_password = db.Column(db.String(255), nullable=False)
    m_firstname = db.Column(db.String(255))
    m_lastname = db.Column(db.String(255))
    m_email = db.Column(db.String(255), unique=True)
    m_address = db.Column(db.String(255))
    m_invalid_reason = db.Column(db.String(1))
    m_is_admin = db.Column(db.Integer)
    m_signin_datetime = db.Column(db.DateTime(), default=db.func.now())
    m_valid_start_datetime = db.Column(db.DateTime(), default=datetime.now())
    m_valid_end_datetime = db.Column(
        db.DateTime(), default=datetime.now() + timedelta(weeks=5200)
    )

    def __init__(
        self,
        m_username,
        m_password,
        m_firstname,
        m_lastname,
        m_email,
        m_address,
        m_is_admin=0,
        m_valid_end_datetime=None,
        m_invalid_reason="D",
    ):

        m_is_admin = 1 if m_is_admin == "Admin" else 0
        self.m_username = m_username
        self.set_password(m_password)
        self.m_firstname = m_firstname.title()
        self.m_lastname = m_lastname.title()
        self.m_email = m_email.lower()
        self.m_address = m_address.lower()
        self.m_is_admin = m_is_admin
        self.m_invalid_reason = m_invalid_reason
        if m_valid_end_datetime != None:
            self.m_valid_end_datetime = m_valid_end_datetime

    def json(self):
        return {
            "m_id": self.id,
            "m_username": self.m_username,
            "m_firstname": self.m_firstname,
            "m_lastname": self.m_lastname,
            "m_email": self.m_email,
            "m_address": self.m_address,
            "m_invalid_reason": self.m_invalid_reason,
            "m_is_admin": "Admin" if self.m_is_admin == 1 else "Not-Admin",
        }

    @classmethod
    def json_users_list(cls):
        users = cls.query.all()
        users = [
            {
                "m_id": user.id,
                "m_username": user.m_username,
                "m_firstname": user.m_firstname,
                "m_lastname": user.m_lastname,
                "m_email": user.m_email,
                "m_address": user.m_address,
                "m_invalid_reason": user.m_invalid_reason,
                "m_is_admin": "Admin" if user.m_is_admin == 1 else "Not Admin",
            }
            for user in users
        ]
        return users

    def set_password(self, m_password):
        self.m_password = generate_password_hash(m_password)

    def check_password(self, m_password):
        return check_password_hash(self.m_password, m_password)

    def upserting(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_id(cls, m_user_id):
        return cls.query.filter_by(id=m_user_id).first()

    @classmethod
    def find_by_username(cls, m_username):
        return cls.query.filter_by(m_username=m_username).first()

    @classmethod
    def exist_email(cls, m_email):
        return cls.query.filter_by(m_email=m_email).first()
