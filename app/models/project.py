from datetime import datetime, timedelta

from app.db import db
from app.models.concept import ConceptModel


class ProjectModel(db.Model):
    __tablename__ = "mapper_project"
    __table_args__ = {"schema": "susana"}

    m_project_id = db.Column(db.Integer, primary_key=True)
    m_project_type_id = db.Column(db.Text(255), primary_key=True)
    m_valid_start_datetime = db.Column(db.Date(), default=datetime.now())  # CURRENT_DATE
    m_valid_end_datetime = db.Column(
        db.Date(), default=datetime.now() + timedelta(weeks=5200)
    )
    m_invalid_reason = db.Column(db.Text())

    concepts = db.relationship(
        "ConceptModel",
        backref="project",
        lazy="dynamic",
        foreign_keys="ConceptModel.m_project_id",
    )
    roles = db.relationship(
        "ConceptModel",
        backref="project",
        lazy="dynamic",
        foreign_keys="ConceptModel.m_project_id",
    )

    def json(self):
        return {
            "m_project_id": self.m_project_id,
            "m_project_type_id": self.m_project_type_id,
        }

    @classmethod
    def find_by_id(cls, m_project_id):
        return cls.query.filter_by(m_project_id=m_project_id).all()
