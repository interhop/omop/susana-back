from datetime import timedelta

from flask_jwt_extended import (
    create_access_token,
    get_jwt_claims,
    get_raw_jwt,
    jwt_required,
)
from flask_restful import Resource, reqparse

from app.blacklist import BLACKLIST
from app.models.user import UserModel


class UserList(Resource):
    @jwt_required
    def get(self):
        users = UserModel.json_users_list()
        return {"users": users, "users_number": len(users)}, 200


class User(Resource):
    parser = reqparse.RequestParser()

    parser.add_argument(
        "m_password", type=str, help="you have to provide a password (str)"
    )
    parser.add_argument(
        "m_firstname",
        type=str,
        required=True,
        help="you have to provide a firsname (str)",
    )
    parser.add_argument(
        "m_lastname", type=str, required=True, help="you have to provide a lastname (str)"
    )
    parser.add_argument(
        "m_email", type=str, required=True, help="you have to provide an email (str)"
    )
    parser.add_argument(
        "m_address", type=str, required=True, help="you have to provide a address (str)"
    )
    parser.add_argument("m_invalid_reason", type=str, default="D")
    parser.add_argument("m_is_admin", type=str, default="Not-Admin")

    @classmethod
    @jwt_required
    def get(cls, m_username):
        user = UserModel.find_by_username(m_username)
        if not user:
            return {"message": "User not found"}, 404
        return user.json(), 200

    @classmethod
    def post(cls, m_username):
        data = cls.parser.parse_args()
        data["m_is_admin"] = 1 if data["m_is_admin"] == "Admin" else 0

        if UserModel.find_by_username(m_username):
            return {"message": "A user with that username already exists"}, 400
        if UserModel.exist_email(data["m_email"]):
            return {"message": "This email already exists with another users!"}, 400

        user = UserModel(m_username, **data)
        user.upserting()
        return {"message": "User created"}, 201

    @classmethod
    @jwt_required
    def put(cls, m_username):
        data = User.parser.parse_args()
        data["m_is_admin"] = 1 if data["m_is_admin"] == "Admin" else 0

        claims = get_jwt_claims()
        user = UserModel.find_by_username(m_username)

        if claims["m_is_admin"] == 1 or user.m_username == claims["m_username"]:
            if not user:
                user = UserModel(m_username, **data)
            else:
                for k in data.keys():
                    if k != "m_password":
                        setattr(user, k, data[k])
                if data["m_password"]:
                    user.set_password(data["m_password"])
            user.upserting()
            return {"message": "User created/updated"}, 200
        return {"message": "Not authorized"}, 401  # Unauthorized


#    @classmethod
#    @jwt_required
#    def delete(cls, m_username):
#        claims = get_jwt_claims()
#        if claims['m_is_admin'] == 1:
#            user = UserModel.find_by_username(m_username)
#            if not user:
#                return {'message': 'User not found'}, 404
#            user.m_invalid_reason = 'D'
#            user.m_valid_end_date = datetime.now()
#            user.upserting()
#            return {'message': "User deleted (invalid_reason = 'D')"}, 200
#        return {'message': 'Not authorized'}, 401 # Unauthorized


class UserLogin(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        "m_username", type=str, required=True, help="you have to provide a username (str)"
    )
    parser.add_argument(
        "m_password", type=str, required=True, help="you have to provide a password (str)"
    )

    @classmethod
    def post(cls):
        data = cls.parser.parse_args()

        user = UserModel.find_by_username(data["m_username"])
        if (
            user
            and user.check_password(data["m_password"])
            and user.m_invalid_reason != "D"
        ):
            expires = timedelta(days=1)
            access_token = create_access_token(
                identity=user, fresh=True, expires_delta=expires
            )
            return {
                "access_token": access_token,
                "m_username": user.m_username,
                "m_is_admin": "Admin" if user.m_is_admin == 1 else "Not-Admin",
            }, 200

        return {"message": "Invalid credentials"}, 401  # Unauthorized


class UserLogout(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()["jti"]  # jti is "JWT ID", a unique identifier for a JWT.
        BLACKLIST.add(jti)
        return {"message": "Successfully logged out"}, 200
