import json
import re

from flask import request
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restful import Resource, reqparse

from app.models.concept import ConceptAncestorModel, ConceptModel, RoleModel
from app.models.job import JobModel
from app.models.synonym import SynonymModel
from app.solralgo.SolrAlgo import solr_algo_frequency, solr_algo_simple, solr_query_multi
from conf_files.config import SOLR_AUTH, SOLR_CORE, SOLR_HOST, SOLR_PORT
from pysolrwrapper.core import SolrQuery
from pysolrwrapper.filter import FilterColumnEnum, FilterColumnNum
from share.macros import Algo, Column, Language

TO_FILTER = [
    "domain_id",
    "vocabulary_id",
    "concept_class_id",
    "standard_concept",
    "invalid_reason",
    "is_mapped",
    "m_language_id",
]
SEPARATOR = " - "

# but
# ajouter les facets identiques ensemble sauf m_project_type_id


def sum_facets(non_filter_facets):
    filter_facets = {}
    for facet_key, facet_value in non_filter_facets.items():
        if facet_key == "m_project_type_id":
            filter_facets[facet_key] = facet_value
        else:
            result = {}
            for elem in facet_value:
                for dico in elem:
                    city = dico.split(SEPARATOR)
                    key = SEPARATOR.join(city[1:])
                    if result.get(key, None):
                        result[key] += elem[dico]
                    else:
                        result[key] = elem[dico]
            result_list = []
            for k in result:
                result_list.append({k: result[k]})
            filter_facets[facet_key] = result_list
    return filter_facets


def erase_projects_name(docs):
    for doc in docs:
        if not doc.get("m_project_type_id", None):
            print(doc)
        for k in doc:
            if k in TO_FILTER:
                regex = "^" + doc["m_project_type_id"] + SEPARATOR
                doc[k] = re.sub(regex, "", doc[k])


def add_project_prefix(filters, projects):
    ret = []
    for filter in filters:
        for project in projects:
            ret.append(project.lower() + SEPARATOR + filter.lower())
    return ret


class ConceptList(Resource):
    """
    endpoint : /concepts/
    """

    @classmethod
    @jwt_required
    def get(cls):
        data = request.args.get("json")
        data = json.loads(data)

        search_string = data.get("search_string", None)
        non_translate_string = data.get("non_translate_string", None)
        non_translate_string_language = data.get("non_translate_string_language", None)
        mini = data.get("mini", None)
        maxi = data.get("maxi", None)
        select = data.get("select", ["*", "score"])
        sort_order = data.get("sort_order", "desc")
        frequency = data.get("frequency", None)
        algo = data.get("algo", "simple")
        sort_column = Column(int(data.get("sort_column", "0"))).name
        limit = data.get("limit", 15)
        page = data.get("page", 1)
        m_user_id = get_jwt_identity()
        m_project_type_id = data.get("m_project_type_id", [])
        columns_facets = [
            "m_project_type_id",
            "m_language_id",
            "is_mapped",
            "vocabulary_id",
            "domain_id",
            "standard_concept",
            "invalid_reason",
            "m_project_type_id",
        ]
        print("m_project_type_id : ", m_project_type_id)
        # matches allow to only show the projects user is allowed to see
        # projects is a projects filter
        matches = RoleModel.projects_type_id_allowed(m_user_id, "READ")
        # matches = RoleModel.projects_type_id_allowed(m_user_id, 'READ') + ['OMOP']
        if m_project_type_id:
            projects = [
                project_id
                for project_id in m_project_type_id
                if project_id.upper() in RoleModel.projects_type_id_allowed(m_user_id, "READ")
            ]
            # if 'OMOP'.lower() in m_project_type_id:
            #    projects += ['OMOP']
        else:
            projects = matches
        print("projectsss : ", projects)
        projects = [p.lower() for p in projects]
        matches = ["^" + p.lower() + ".*" for p in matches]

        result = (
            SolrQuery(host=SOLR_HOST, port=SOLR_PORT, core=SOLR_CORE, auth=SOLR_AUTH)
            .highlight(["concept_name"])
            .select(select)
            .facet(columns_facets, -1, missing=False, matches=matches)
            .set_type_edismax()
            .limit(limit)
            .sort(sort_column, sort_order)
            .page(page)
        )

        if search_string and non_translate_string_language == "FR" and non_translate_string:
            result = result.query(
                solr_query_multi(
                    fr=data["non_translate_string"],
                    en=data["search_string"],
                    fr_en=data["search_string"],
                )
            )
        elif search_string:
            result = result.query(
                solr_query_multi(
                    fr=data["search_string"],
                    en=data["search_string"],
                    fr_en=data["search_string"],
                )
            )

        if algo == "simple":
            result = result.algo(solr_algo_simple())
        elif algo == "frequency":
            print("frequency_algo: ", frequency)
            result = result.algo(solr_algo_frequency(frequency))

        if mini or maxi:
            result = result.add_where(FilterColumnNum(column="concept_id", inf=mini, sup=maxi))

        if hasattr(data.get("standard_concept"), "__iter__"):
            for n, i in enumerate(data.get("standard_concept")):
                if i == "Standard":
                    data.get("standard_concept")[n] = "S"
                elif i == "Classification":
                    data.get("standard_concept")[n] = "C"
                elif i == "Non-Standard":
                    data.get("standard_concept")[n] = "EMPTY"

        if hasattr(data.get("invalid_reason"), "__iter__"):
            for n, i in enumerate(data.get("invalid_reason")):
                if i == "Deleted":
                    data.get("invalid_reason")[n] = "D"
                elif i == "Updated":
                    data.get("invalid_reason")[n] = "U"
                elif i == "Valid":
                    data.get("invalid_reason")[n] = "EMPTY"

        if hasattr(data.get("is_mapped"), "__iter__"):
            for n, i in enumerate(data.get("is_mapped")):
                if i == "Mapped":
                    data.get("is_mapped")[n] = "S"
                #                elif i == 'Non-Standard':
                #                    data.get('is_mapped')[n] = 'NS'
                elif i == "Not-Mapped":
                    data.get("is_mapped")[n] = "EMPTY"

        data = {
            k: data[k]
            for k in data
            if k
            in (
                "invalid_reason",
                "standard_concept",
                "m_project_type_id",
                "domain_id",
                "vocabulary_id",
                "m_language_id",
                "m_frequency_id",
                "is_mapped",
            )
        }

        print(data)

        for k, v in data.items():
            if v != []:
                if k in TO_FILTER:
                    v = add_project_prefix(v, projects)
                print("ret", k, v)
                result = result.add_where(FilterColumnEnum(k, v))

        result = result.run()

        erase_projects_name(result["docs"])
        result["facet_fields"] = sum_facets(result["facet_fields"])

        return result, 200


class Concept(Resource):
    """
    endpoint : /concept/<int:_id>
    """

    parser = reqparse.RequestParser()
    parser.add_argument("concept_name", type=str, required=True, help="This filed cannot be left blank!")
    parser.add_argument("domain_id", type=str, required=True, help="This filed cannot be left blank!")
    parser.add_argument("vocabulary_id", type=str)
    parser.add_argument("standard_concept", type=str)
    parser.add_argument("concept_code", type=str)
    parser.add_argument("concept_class_id", type=str)
    parser.add_argument("invalid_reason", type=str)
    parser.add_argument("valid_end_date", type=str)
    parser.add_argument("m_frequency_id", type=str)
    parser.add_argument("m_language_id", type=str)
    parser.add_argument("m_project_id", type=str)
    parser.add_argument("relations", action="append")

    @classmethod
    @jwt_required
    def get(cls, _id):
        """
        concept_id, concept_name
        et on a ses relation (list): concept_id, concept_name

        if _id doesnt exist : error "Item not found"
        """
        m_user_id = get_jwt_identity()
        concept = ConceptModel.find_by_id(_id)

        if not concept:
            return {"message": "Item not found"}, 404
        if not RoleModel.read(_id, m_user_id):
            return {"message": "Unauthorized"}, 401

        ret = concept.json()

        print(_id)
        # check if GoogleTrans API already provide a synonym for this concept in english (no versioning here)
        # currently, OMOP supports only English Synonyms (concept_id == Language.ENGLISH.value)
        find_synonym = SynonymModel.find_synonym(
            _id,
            language_concept_id=Language.ENGLISH.value,
            m_algo_id=[Algo.AUTOMATIC_TRANS.value, Algo.FROM_OTHER_PROJECTS_TRANS.value],
        )

        print(_id)
        if find_synonym and ret["m_language_id"] and ret["m_language_id"].lower() != "en":
            print("found but not english")
            ret["translate_api_en"] = find_synonym.concept_synonym_name

        print(_id)
        ret["synonyms"] = ", ".join([q.concept_synonym_name for q in SynonymModel.find_by_id(_id)])
        ret["relations"] = ConceptModel.find_concept_relation_by_id(_id)
        ret["ancestor"] = ConceptAncestorModel.find_ancestor_by_id(_id)
        ret["descendant"] = ConceptAncestorModel.find_descendant_by_id(_id)

        # new_job = JobModel(_id, 'SYNONYM_TRANSLATION_API_EXTEND', m_user_id)
        new_job = JobModel(_id, "BULK_TRANSLATION", m_user_id)
        new_job.upserting()

        return ret, 200
