# Installation

## Dependencies

- docker
- docker-compose
- nginx
- postgresql 13 (otherwise use the provided postgres docker)

## Procedure

- clone the repository
- duplicate the docker-compose-prod.yml
- create a `susana` database, user and schema
- initialize the postgres database with `resources/postgresql/dump.sql`
- load your terminologies
- copy the `env.conf` into `.env` and replace with your own values
- configure nginx
- `docker-compose -f docker-compose-<your-env>.yml up -d`
