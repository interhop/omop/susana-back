Introduction
============
[Susana](http://susana.interhop.org) is a web based application created by French Paris hospitals to map source terminologies to standard one as defined by the Observational Health Data Sciences and Informatics (OHDSI) teams. 
It is a collaborative, international, user-based and ergonomic website. 

It’s combine 
- a free text research tool
- a mapping user-based tool

As a collaborative tool, [Susana](http://susana.interhop.org) allows the convergence of terminology between databases and languages.

The key stone : OHDSI / OMOP
===========================
We use [Observational Medical Outcomes Partnership (OMOP) Vocabulary](http://www.ohdsi.org/data-standardization/vocabulary-resources/) to standardize terminologies and all the OMOP terminologies has been loaded.
We use [OMOP-CDM](http://www.ohdsi.org/data-standardization/the-common-data-model/) as database structure and provide some updates needed (see the following schema)
<img src="images/omop-mapper_2019_04_05.png"  width="60%">



Process Overview
===============
You don’t have to install anything.

1. Source codes that needs mapping have to be pushed to this framagit : [https://framagit.org/interchu/conceptual-mapping/terminologies](terminologies)
- Framagit allowed versioning, storage and sharing of terminologies. As it is sensitive data, this framagit is private.
- We provide template to understand how to fill source code.
- Local terminologies will be loaded in Susana in few days. 

2. Register in [Susana](http://susana.interhop.org) 
3. When your registration in accepted, you can login and explore your terminology with filters and full text research.
4. You can map items with full text reseach and automatic translation. Term similarities algorithms approach are used to connect source codes to Vocabulary concepts. 
[Susana](http://susana.interhop.org) will propose concepts that are marked as standard concepts in the Vocabulary and are in relation to the current domain.
You can change this to map to other non standard concept (intralanguage mapping)

Export the mapped concepts
==========================

The `terminologies/export/` folder contains the exported tables for every projects. In particular it contains those:
- `concept`
- `concept_relationship`
- `concept_synonym`
- `pivot`

the pivot table is particularly usefull for your ETL. It defines which standard
concept is mapped to which one. Also it contains the vote information. You
might use it to build your ETL and get both local concepts and standard
concepts.

Source code
===========
All source code is freely available:
- [frontend](https://framagit.org/interchu/concept_mapper_front)<br/>
- [backend](https://framagit.org/interchu/omop-omop-back)<br/>
<img src="images/global_archi_omop-mapper.png" width="30%"><br/>
Fell free to ask question and write [issues](https://framagit.org/interchu/conceptual-mapping/issues)

Useful functions of the website
==============================
- First connexion<br/>
![Database Structure](images/log.png)<br/>
![Database Structure](images/registration.png)<br/>
Sign in and wait the validation's account of Susana team.
You may push your local terminologies in framagit to be able to explore it.
All the terminologies are available by anyone registered in susana.interhop.org

- Frequencies
The frequencies of your local terminologies are used to prioritize the mapping. Their are not mandatory.
As it may be sensitive informations, a semi quantitive approches is used and you don't have to give the exact count

- Free text research and filters
![Research](images/searchPage.png)
As you can see there are filters (some are in relation to OMOP CDM (vocabulary, domain), some are specific to susana.interhop.org)
You may choose your hospital project.
The green banner means that the item is mapped to a standard item
[susana](http://susana.interhop.org) support structured search queries, live boolean operators like AND and OR or wildcard research

- Concept detail window
![goToConceptDetailPage](images/goToConceptDetailPage.png)
You only have to click to a concept row to explore concept details.
![ConceptDetail](images/conceptDetailsPage.png)

- Mapping tool window
![goToMapperPage](images/goToMapperPage.png)
You have to select items and click on the red button to map.
If you select many items you can do many to one mapping.
Otherwise you will do one to one mapping (one source concept will be map to one standard concept).
![mapperPage](images/mapperPage.png)
Susana process to an automatic mapping and the mapping page provide the list of the best matches.<br/>
The mapping features are :
   - Full history of mapping process : audit-log
   - Search and filter in code systems with the ability to change filters and launch an new requests
   - Concordance Match: automatically finds the best match in the target (~standard) and source (~local) codes
You can add comments and rate to the relation you are about to map.<br/>
You can undo relationship mapping with the concept detail window.<br/>

- Review tool window
![goToReviewerPage](images/goToReviewerPage.png)
You you may review previous mapping.
![reviewerPage](images/reviewerPage.png)
You may upvote or downvote previous mapping!

- **Keyboard shortcuts:**

    *Concept search screen*
    - `CTRL + K` = go to the search bar
    - `CTRL + up/down` = choose a source concept to map
    - `CTRL + M` = go to the mapping screen
    - `CTRL + right/left` = go to previous / next concepts
    - `CTRL + F` = open filters
    
    *mapping screen*
    - `CTRL + K` = go to the search bar
    - `CTRL + up/down` = choose a target concept to map
    - `CTRL + right/left` = go to previous / next concepts
    - `CTRL + number` = choose a target concept to map (ex CTRL + 1: map the concept to the 1st target concept)
    
    *Mapping review screen*
    - `CTRL + switch` to concept id = open concept pop-up

Languages
=========
{ 
    'af' : 4180185      ,  # Afrikaans language  
    'sq' : 4182497      ,  # Albanian language  
    'am' : 4182354      ,  # Amharic language  
    'ar' : 4181374      ,  # Arabic language  
    'hy' : 4180063      ,  # Armenian language  
    'az' : 4181383      ,  # Azerbaijani language  
    'eu' : 4175770      ,  # Basque language  
    'be' : 4180194      ,  # Belorussian language  
    'bn' : 4052786      ,  # Bengali language  
    'bs' : 40481563     ,  # Bosnian language  
    'bg' : 4182512      ,  # Bulgarian language  
    'ca' : 4181535      ,  # Catalan language  
    'zh-cn': 4182948    ,  # Chinese language  
    'co' : 4232095      ,  # Corsican language  
    'hr' : 40481973     ,  # Croatian language  
    'cs' : 4180197      ,  # Czech language  
    'da' : 4180183      ,  # Danish language  
    'nl' : 4182503      ,  # Dutch language  
    'en' : 4180186      ,  # English language  
    'eo' : 4178686      ,  # Esperanto  
    'et' : 4183943      ,  # Estonian language  
    'fi' : 4181730      ,  # Finnish language  
    'fr' : 4180190      ,  # French language  
    'fy' : 4180188      ,  # Frisian language  
    'gl' : 4261080      ,  # Galician language  
    'ka' : 4182481      ,  # Georgian language  
    'de' : 4182504      ,  # German language  
    'el' : 4180189      ,  # Greek language  
    'gu' : 4059174      ,  # Gujarati language  
    'ht' : 44802876     ,  # Haitian Creole language  
    'ha' : 4178556      ,  # Hausa language  
    'haw' : 4181557     ,  # Hawaiian language  
    'iw' : 4180047      ,  # Hebrew language  
    'hi' : 4052346      ,  # Hindi language  
    'hmn' :  46270898   ,  # Hmong language  
    'hu' : 4183945      ,  # Hungarian language  
    'is' : 4175776      ,  # Icelandic language  
    'id' : 4183663      ,  # Indonesian language  
    'ga' : 4180182      ,  # Irish Gaelic language  
    'it' : 4182507      ,  # Italian language  
    'ja' : 4181524      ,  # Japanese language  
    'jw' : 4175914      ,  # Javanese language  
    'kk' : 4182474      ,  # Kazakh language  
    'km' : 4183770      ,  # Khmer language  
    'ko' : 4175771      ,  # Korean language  
    'ku' : 4175909      ,  # Kurdish language  
    'lo' : 4182952      ,  # Lao language  
    'la' : 4181534      ,  # Latin language  
    'lv' : 4180064      ,  # Latvian language  
    'lt' : 4175772      ,  # Lithuanian language  
    'lb' : 46285414     ,  # Main spoken language Luxembourgish  
    'mk' : 4175905      ,  # Macedonian language  
    'mg' : 4180215      ,  # Malagasy language  
    'ms' : 4183666      ,  # Malay language  
    'ml' : 4180058      ,  # Malayalam language  
    'mt' : 4175735      ,  # Maltese language  
    'mi' : 4180223      ,  # Maori language  
    'mr' : 4182519      ,  # Marathi language  
    'mn' : 4182357      ,  # Mongolian language  
    'my' : 4181727      ,  # Burmese language  
    'ne' : 4175908      ,  # Nepali language  
    'no' : 4181530      ,  # Norwegian language  
    'fa' : 4183659      ,  # Persian language  
    'pl' : 4182515      ,  # Polish language  
    'pt' : 4181536      ,  # Portuguese language  
    'pa' : 4059175      ,  # Punjabi language  
    'ro' : 4181537      ,  # Rumanian language  
    'ru' : 4181539      ,  # Russian language  
    'sm' : 4181558      ,  # Samoan language  
    'gd' : 4181529      ,  # Scottish Gaelic language  
    'sr' : 40483799     ,  # Serbian language  
    'sn' : 4175936      ,  # Shona language  
    'sd' : 4181545      ,  # Sindhi language  
    'si' : 4183657      ,  # Sinhalese language  
    'sk' : 4181540      ,  # Slovak language  
    'sl' : 4182513      ,  # Slovenian language  
    'so' : 4182350      ,  # Somali language  
    'es' : 4182511      ,  # Spanish language  
    'su' : 4181554      ,  # Sundanese language  
    'sw' : 4181698      ,  # Swahili language  
    'sv' : 4175777      ,  # Swedish language  
    'tg' : 4181547      ,  # Tajik language  
    'ta' : 4181521      ,  # Tamil language  
    'te' : 4175767      ,  # Telugu language  
    'th' : 4183934      ,  # Thai language  
    'tr' : 4175744      ,  # Turkish language  
    'uk' : 4175904      ,  # Ukrainian language  
    'ur' : 4059788      ,  # Urdu language  
    'uz' : 4175743      ,  # Uzbek language  
    'vi' : 4181526      ,  # Vietnamese language  
    'cy' : 4175774      ,  # Welsh language  
    'xh' : 4180355      ,  # Xhosa language  
    'yi' : 4181531      ,  # Yiddish language  
    'yo' : 4183798      ,  # Yoruba language  
    'zu' : 4181699      ,  # Zulu language  
    'he' : 4180047         # Hebrew language  
}  

ToDo
====
- implement others algorithms to help mapping : units
- project and role management
