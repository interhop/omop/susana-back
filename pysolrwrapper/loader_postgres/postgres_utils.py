import logging
import tempfile
from dataclasses import dataclass

import re
import psycopg2


@dataclass
class PostgresConf:
    host: str
    port: int
    database: str
    user: str
    password: str

    def __repr__(self) -> str:
        return f"postgresql://{self.user}:{self.password}@{self.host}:{self.port}/{self.database}"


class PostgresClient:
    def __init__(self, conn):
        self.postgres_conf = PostgresConf(
            user=conn.info.user,
            password=conn.info.password,
            port=conn.info.port,
            database=conn.info.dbname,
            host=conn.info.host,
        )
        self.conn = conn

    @classmethod
    def from_conf(cls, postgres_conf: PostgresConf):
        conn = psycopg2.connect(dsn=str(postgres_conf))
        conn.set_session(autocommit=True)
        return cls(conn)

    def run(self, sql) -> None:
        with self.conn.cursor() as cur:
            cur.execute(sql)
            logging.info("%s row affected", str(cur.rowcount))
            logging.debug("%s", sql)
            return cur.rowcount

    def bulk_export(self, query, file) -> None:
        with self.conn.cursor() as cur:
            with open(file, "w") as file:
                cur.copy_expert(f"COPY ({query})  TO STDOUT WITH CSV HEADER", file)

    def bulk_load(self, table, path, delimiter=",", quote='"', header="HEADER") -> None:
        logging.info("loading %s from %s", table, path)
        with self.conn.cursor() as cur:
            with open(path, "r", buffering=8192) as file:
                cur.copy_expert(
                    f"""COPY "{table}"  FROM STDIN WITH CSV {header} DELIMITER E'{delimiter}' QUOTE E'{quote}' """,
                    file,
                )

    def bulk_load_pandas(self, df, table) -> None:
        with self.conn.cursor() as cur, tempfile.NamedTemporaryFile(mode="w") as path:
            logging.info("loading %s from %s", table, path.name)
            df.to_csv(path.name, index=False)
            with open(path.name, "r", buffering=8192) as file:
                cur.copy_expert(
                    f'COPY "{table}" ({",".join(df.columns)})  FROM STDIN WITH CSV HEADER',
                    file,
                )
            logging.info("Bulk loaded %s rows into %s", len(df.index), table)

    def bulk_export_solr(self, query: str, file, sep1: str = "\t") -> None:
        with self.conn.cursor() as cur:
            with open(file, "w") as file:
                cur.copy_expert(
                    f"COPY ({query})  TO STDOUT CSV HEADER DELIMITER '{sep1}' ENCODING 'UTF8' NULL AS ''",
                    file,
                )


def rename(table_name: str):
    return re.sub("^\d+_", "", table_name)
