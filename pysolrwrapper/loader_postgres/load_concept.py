import logging
from pathlib import Path

import click

from conf_files.config import POSTGRES
from pysolrwrapper.loader_postgres.postgres_utils import PostgresClient, PostgresConf
from pysolrwrapper.loader_postgres.susana_loader import SusanaLoader


@click.command()
@click.option("--project-type", help="Project acronym. Can be a new or existing one", required=True)
@click.option("--vocabulary", help="Vocabulary name", required=True)
@click.option("--domain", help="Domain name", required=True)
@click.option("--user-id", type=int, help="User id of the uploader user", required=True)
@click.option(
    "--language",
    type=click.Choice(["EN", "FR"], case_sensitive=False),
    help="Language of the concepts",
    required=True,
)
@click.option(
    "--concept-path", type=click.Path(exists=True), help="Relative or absolute path of concept CSV", required=True
)
@click.option(
    "--relationship-path",
    type=click.Path(exists=True),
    help="Relative or absolute path of concept_relationship CSV",
    required=True,
)
@click.option(
    "--refresh",
    type=click.Choice(["ALL", "CONCEPT", "RELATIONSHIP"], case_sensitive=False),
    help="Use only to refresh existing concepts",
)
@click.option("--debug/--no-debug", help="Show debug trace", default=False, show_default=True)
def load(
    project_type,
    vocabulary,
    domain,
    user_id,
    language,
    concept_path,
    relationship_path,
    refresh,
    debug,
):
    """Load concepts, relationship or synonyms"""
    logging.basicConfig(level=logging.INFO)

    pg_client = PostgresClient.from_conf(PostgresConf(**POSTGRES))
    pg_client.run("set search_path to susana")
    concept_path = Path(concept_path) if concept_path else None
    relationship_path = Path(relationship_path) if relationship_path else None
    loader = SusanaLoader(
        pg_client=pg_client,
        m_project_type_id=project_type,
        m_user_id=user_id,
        vocabulary_id=vocabulary,
        domain_id=domain,
        m_language_id=language,
        concept_path=concept_path,
        relationship_path=relationship_path,
        debug=debug,
    )
    if refresh:
        loader.refresh(perimeter=refresh)
    else:
        loader.run()
