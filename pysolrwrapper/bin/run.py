import logging

import click

from pysolrwrapper.loader_postgres.export_concept import export
from pysolrwrapper.loader_postgres.load_concept import load
from pysolrwrapper.loader_postgres.upgrade_concept import upgrade

logging.basicConfig(level=logging.INFO)


@click.group()
def cli():
    """Susana Command Line Interpreter.

    NOTE: This assumes postgres connection specified into .env
    """
    pass


cli.add_command(load)
cli.add_command(export)
cli.add_command(upgrade)

if __name__ == "__main__":
    cli()
