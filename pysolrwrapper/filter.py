class Filter:
    pass


class FilterText(Filter):
    def __init__(self, column: str, value: [str]):
        self._column = column
        self._value = value

    def __repr__(self):
        lst = " OR ".join(self._value)
        return f"{self._column}:({lst})"


class FilterColumnEnum(Filter):
    def __init__(self, column: str, value: [str]):
        self._column = column
        self._value = value

    def __repr__(self):
        lst = " OR ".join('"%s"' % x for x in self._value)
        return f"{self._column}:({lst})"


class FilterColumnNum(Filter):
    def __init__(self, column: str, inf: int = "*", sup: int = "*"):
        if not inf:
            inf = "*"
        if not sup:
            sup = "*"
        self._column = column
        self._inf = inf
        self._sup = sup

    def __repr__(self):
        return f"{self._column}:[{self._inf} TO {self._sup}]"


def AND(*filters: Filter):
    tmp = " AND ".join([str(x) for x in filters])
    return tmp


def OR(*filters: Filter):
    tmp = " OR ".join([str(x) for x in filters])
    return f"({tmp})"


def NOT(*filters: Filter):
    tmp = "NOT (" + AND(*filters) + ")"
    return tmp
