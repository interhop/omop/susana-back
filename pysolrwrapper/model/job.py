from collections import Sequence
from dataclasses import dataclass

from sqlalchemy import BigInteger, Boolean, DateTime, ForeignKey, Text
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy.testing.schema import Column

from pysolrwrapper.model.base import Base


@dataclass
class Job(Base):
    __tablename__ = "job"
    job_id = Column(BigInteger, Sequence("job_id_seq"), primary_key=True, index=True)
    host_dsn = Column(Text)
    schema_name = Column(Text)
    solr_core = Column(Text)
    state = Column(JSONB)
    is_active = Column(Boolean)

    events = relationship("JobEvent", back_populates="jobs")


class JobEvent(Base):
    __tablename__ = "job_event"
    job_event_id = Column(
        BigInteger, Sequence("job_id_seq"), primary_key=True, index=True
    )
    job_id = Column(BigInteger, ForeignKey("job.job_id"), index=True)
    start_datetime = Column(DateTime)
    end_datetime = Column(DateTime)
    duration_millis = Column(BigInteger)
    error_message = Column(Text)

    jobs = relationship("Job", back_populates="events")
