-- name: drop_concept_table#
-- create unlogged table to load the new concepts in
DROP TABLE IF EXISTS tmp_concept
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_vocabulary
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_domain
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_concept_class
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_concept_relationship
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_relationship
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_concept_synonym
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_concept_ancestor
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_source_to_concept_map
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_drug_strength
;


--HINT DISTRIBUTE ON RANDOM
DROP TABLE IF EXISTS tmp_attribute_definition
;

DROP TABLE IF EXISTS tmp_job
;
