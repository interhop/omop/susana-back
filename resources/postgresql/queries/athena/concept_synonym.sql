-- name: insert_concept_synonym!
with ins as (
insert into concept_synonym (
  concept_id			,
  concept_synonym_name		,
  language_concept_id
)
select
  t.concept_id			,
  t.concept_synonym_name		,
  t.language_concept_id
from tmp_concept_synonym as t
left join concept_synonym v on v.concept_id = t.concept_id
where v.concept_id is null
returning concept_id
)
insert into tmp_job (concept_id)
select concept_id from ins;

-- name: update_concept_synonym!
-- no primary key, skipping
