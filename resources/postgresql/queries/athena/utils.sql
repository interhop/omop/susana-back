-- name: begin_transaction!
-- dummy comment
begin;

-- name: create_job_table#
-- create unlogged table to load the concepts to be updated in solr
CREATE UNLOGGED TABLE tmp_job (concept_id int);

-- name: deactivate_foreign_key!
-- dummy comment
select 1;



-- name: reactivate_foreign_key
-- dummy comment
select 1;

-- name: insert_job!
-- dummy comment
insert into mapper_job (m_concept_id, m_job_type_id, m_status_id)
select distinct concept_id, 'INDEXATION', 0
from tmp_job
UNION ALL
select distinct concept_id, 'BULK_TRANSLATION', 0
from tmp_job;

-- name: commit_transaction!
-- dummy comment
commit;

-- name: test
-- dummy comment
select * from mapper_job;
