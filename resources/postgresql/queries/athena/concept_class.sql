-- name: insert_concept_class!
insert into concept_class (
  concept_class_id			,
  concept_class_name		,
  concept_class_concept_id
)
select
  t.concept_class_id			,
  t.concept_class_name		,
  t.concept_class_concept_id
from tmp_concept_class as t
left join concept_class v on v.concept_class_id = t.concept_class_id
where v.concept_class_id is null;

-- name: update_concept_class!
update concept_class set
concept_class_name	      = v.concept_class_name		,
concept_class_concept_id = v.concept_class_concept_id
from tmp_concept_class v
where concept_class.concept_class_id = v.concept_class_id
and (
concept_class.concept_class_name	      is distinct from v.concept_class_name		or
concept_class.concept_class_concept_id is distinct from v.concept_class_concept_id
);
