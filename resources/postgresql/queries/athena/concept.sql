-- name: insert_concept!
with ins as (
insert into concept (
  concept_id		,
  concept_name		,
  domain_id			,
  vocabulary_id		,
  concept_class_id	,
  standard_concept  ,
  concept_code		,
  valid_start_date	,
  valid_end_date	,
  invalid_reason    ,
  m_language_id     ,
  m_project_id
)
select
  t.concept_id		    ,
  t.concept_name		,
  t.domain_id			,
  t.vocabulary_id		,
  t.concept_class_id	,
  t.standard_concept    ,
  t.concept_code		,
  t.valid_start_date	,
  t.valid_end_date	    ,
  t.invalid_reason      ,
  'EN'                  ,
  8 -- OMOP Project
from tmp_concept t
left join concept c on c.concept_id = t.concept_id
where t.concept_id is null
returning concept_id
)
insert into tmp_job (concept_id)
select concept_id from ins;

-- name: update_concept!
with upd as (
update concept  set
concept_name	   = t.concept_name		,
domain_id		   = t.domain_id			,
vocabulary_id	   = t.vocabulary_id		,
concept_class_id = t.concept_class_id	,
standard_concept = t.standard_concept  ,
concept_code	   = t.concept_code		,
valid_start_date = t.valid_start_date	,
valid_end_date   = t.valid_end_date	,
invalid_reason   = t.invalid_reason
from tmp_concept as t
where concept.concept_id = t.concept_id
and
(
concept.concept_name	   is distinct from t.concept_name	  or
concept.domain_id		   is distinct from t.domain_id		  or
concept.vocabulary_id	   is distinct from t.vocabulary_id	  or
concept.concept_class_id is distinct from t.concept_class_id  or
concept.standard_concept is distinct from t.standard_concept  or
concept.concept_code	   is distinct from t.concept_code	  or
concept.valid_start_date is distinct from t.valid_start_date  or
concept.valid_end_date   is distinct from t.valid_end_date    or
concept.invalid_reason   is distinct from t.invalid_reason )
returning concept.concept_id
)
insert into tmp_job (concept_id)
select concept_id from upd;
