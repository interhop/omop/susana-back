-- name: insert_relationship!
insert into relationship (
  relationship_id  ,
  relationship_name  ,
  is_hierarchical  ,
  defines_ancestry  ,
  reverse_relationship_id  ,
  relationship_concept_id
)
select
  t.relationship_id  ,
  t.relationship_name  ,
  t.is_hierarchical  ,
  t.defines_ancestry  ,
  t.reverse_relationship_id  ,
  t.relationship_concept_id
from tmp_relationship as t
left join relationship r on t.relationship_id = r.relationship_id
where r.relationship_id is null;

-- name: update_relationship!
update relationship set
relationship_name  = v.relationship_name  ,
is_hierarchical  = v.is_hierarchical  ,
defines_ancestry  = v.defines_ancestry  ,
reverse_relationship_id = v.reverse_relationship_id  ,
relationship_concept_id = v.relationship_concept_id
from tmp_relationship v
where relationship.relationship_id = v.relationship_id
and (
relationship.relationship_name  is distinct from v.relationship_name  or
relationship.is_hierarchical  is distinct from v.is_hierarchical  or
relationship.defines_ancestry  is distinct from v.defines_ancestry  or
relationship.reverse_relationship_id is distinct from v.reverse_relationship_id  or
relationship.relationship_concept_id is distinct from v.relationship_concept_id
);
