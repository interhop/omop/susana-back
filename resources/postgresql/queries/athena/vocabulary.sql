-- name: insert_vocabulary!
insert into vocabulary (
  vocabulary_id			,
  vocabulary_name		,
  vocabulary_reference	,
  vocabulary_version	,
  vocabulary_concept_id
)
select
  t.vocabulary_id			,
  t.vocabulary_name		,
  t.vocabulary_reference	,
  t.vocabulary_version	,
  t.vocabulary_concept_id
from tmp_vocabulary as t
left join vocabulary v on v.vocabulary_id = t.vocabulary_id
where v.vocabulary_id is null;

-- name: update_vocabulary!
update vocabulary set
vocabulary_name	      = v.vocabulary_name		,
vocabulary_reference  = v.vocabulary_reference	,
vocabulary_version    = v.vocabulary_version	,
vocabulary_concept_id = v.vocabulary_concept_id
from tmp_vocabulary v
where vocabulary.vocabulary_id = v.vocabulary_id
and (
vocabulary.vocabulary_name	      is distinct from v.vocabulary_name		or
vocabulary.vocabulary_reference  is distinct from v.vocabulary_reference	or
vocabulary.vocabulary_version    is distinct from v.vocabulary_version	    or
vocabulary.vocabulary_concept_id is distinct from v.vocabulary_concept_id
);
