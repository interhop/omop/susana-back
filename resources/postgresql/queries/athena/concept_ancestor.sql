-- name: insert_concept_ancestor!
insert into concept_ancestor (
  ancestor_concept_id        ,
  descendant_concept_id      ,
  min_levels_of_separation   ,
  max_levels_of_separation
)
select
  t.ancestor_concept_id			,
  t.descendant_concept_id		,
  t.min_levels_of_separation    ,
  t.max_levels_of_separation
from tmp_concept_ancestor as t
left join concept_ancestor v on (
v.ancestor_concept_id = t.ancestor_concept_id and
v.descendant_concept_id = t.descendant_concept_id
)
where v.ancestor_concept_id is null;

-- name: update_concept_ancestor!
update concept_ancestor set
descendant_concept_id	      = v.descendant_concept_id		,
min_levels_of_separation = v.min_levels_of_separation       ,
max_levels_of_separation = v.max_levels_of_separation
from tmp_concept_ancestor v
where (concept_ancestor.ancestor_concept_id = v.ancestor_concept_id and
concept_ancestor.descendant_concept_id = v.descendant_concept_id
)
and (
concept_ancestor.min_levels_of_separation is distinct from v.min_levels_of_separation       or
concept_ancestor.max_levels_of_separation is distinct from v.max_levels_of_separation
);
