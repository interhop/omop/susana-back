SET statement_timeout = 0;

SET lock_timeout = 0;

SET idle_in_transaction_session_timeout = 0;

SET client_encoding = 'UTF8';

SET standard_conforming_strings = ON;


SET check_function_bodies = FALSE;

SET xmloption = content;

SET client_min_messages = warning;

SET row_security = OFF;

CREATE SCHEMA susana;

SELECT
    pg_catalog.set_config('search_path', 'susana', FALSE);

SET default_tablespace = '';

SET default_table_access_method = heap;

CREATE TABLE attribute_definition (
    attribute_definition_id integer NOT NULL,
    attribute_name text NOT NULL,
    attribute_description text,
    attribute_type_concept_id integer NOT NULL,
    attribute_syntax text
);

CREATE TABLE cdm_source (
    cdm_source_name text NOT NULL,
    cdm_source_abbreviation text,
    cdm_holder text,
    source_description text,
    source_documentation_reference text,
    cdm_etl_reference text,
    source_release_date date,
    cdm_release_date date,
    cdm_version text,
    vocabulary_version text
);

CREATE SEQUENCE map_omop_concept_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE concept (
    concept_id bigint DEFAULT nextval('map_omop_concept_id_seq'::regclass) NOT NULL,
    concept_name text NOT NULL,
    domain_id text NOT NULL,
    vocabulary_id text NOT NULL,
    concept_class_id text,
    standard_concept text,
    concept_code text NOT NULL,
    valid_start_date date NOT NULL,
    valid_end_date date NOT NULL,
    invalid_reason text,
    m_language_id character varying(255),
    m_project_id integer,
    CONSTRAINT chk_c_concept_name CHECK ((concept_name <> ''::text))
);

CREATE TABLE concept_ancestor (
    ancestor_concept_id integer NOT NULL,
    descendant_concept_id integer NOT NULL,
    min_levels_of_separation integer NOT NULL,
    max_levels_of_separation integer NOT NULL
);

CREATE TABLE concept_class (
    concept_class_id text NOT NULL,
    concept_class_name text NOT NULL,
    concept_class_concept_id integer NOT NULL
);

CREATE TABLE concept_relationship (
    concept_id_1 bigint NOT NULL,
    concept_id_2 bigint NOT NULL,
    relationship_id text NOT NULL,
    valid_start_date date NOT NULL,
    valid_end_date date NOT NULL,
    invalid_reason text,
    m_quality_id integer,
    m_user_id integer,
    m_algo_id integer,
    m_modif_start_datetime timestamp without time zone DEFAULT now() NOT NULL,
    m_modif_end_datetime timestamp without time zone,
    m_mapping_comment text,
    m_mapping_rate integer,
    m_concept_relationship_id integer NOT NULL,
    CONSTRAINT chk_cr_invalid_reason CHECK ((COALESCE(invalid_reason, 'D'::text) = 'D'::text))
);

CREATE SEQUENCE concept_relationship_concept_relationship_id_seq
    AS integer START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE concept_relationship_concept_relationship_id_seq OWNED BY concept_relationship.m_concept_relationship_id;

CREATE TABLE concept_synonym (
    concept_id bigint NOT NULL,
    concept_synonym_name text NOT NULL,
    language_concept_id bigint NOT NULL,
    m_user_id integer,
    m_valid_start_datetime timestamp without time zone DEFAULT now(),
    m_valid_end_datetime timestamp without time zone,
    m_invalid_reason character varying(1),
    m_modif_start_datetime timestamp without time zone DEFAULT now(),
    m_modif_end_datetime timestamp without time zone,
    m_algo_id integer,
    CONSTRAINT chk_csyn_concept_synonym_name CHECK ((concept_synonym_name <> ''::text))
);

CREATE TABLE DOMAIN (
    domain_id text NOT NULL,
    domain_name text NOT NULL,
    domain_concept_id integer NOT NULL
);

CREATE TABLE mapper_algo (
    m_algo_id integer NOT NULL,
    m_label character varying(255) NOT NULL,
    m_url character varying(255),
    m_description text,
    m_algo_type_id text NOT NULL,
    m_user_id integer NOT NULL,
    m_valid_start_datetime timestamp without time zone NOT NULL,
    m_valid_end_datetime timestamp without time zone,
    m_invalid_reason character varying(1)
);

CREATE SEQUENCE mapper_algo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE mapper_algo_id_seq OWNED BY mapper_algo.m_algo_id;

CREATE TABLE mapper_job (
    m_job_id integer NOT NULL,
    m_concept_id bigint NOT NULL,
    m_job_type_id character varying(255) NOT NULL,
    m_status_id integer NOT NULL,
    m_user_id integer DEFAULT 1 NOT NULL
);

CREATE SEQUENCE mapper_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE mapper_job_id_seq OWNED BY mapper_job.m_job_id;

CREATE TABLE mapper_project (
    m_project_id integer NOT NULL,
    m_project_type_id character varying(255) NOT NULL,
    m_valid_start_datetime timestamp without time zone NOT NULL,
    m_valid_end_datetime timestamp without time zone,
    m_invalid_reason character varying(1)
);

CREATE TABLE mapper_role (
    m_project_id integer NOT NULL,
    m_user_id integer NOT NULL,
    m_role_id character varying(255) NOT NULL,
    m_valid_start_datetime timestamp without time zone NOT NULL,
    m_valid_end_datetime timestamp without time zone,
    m_invalid_reason character varying(1)
);

CREATE TABLE mapper_statistic (
    m_statistic_type_id character varying(255) NOT NULL,
    m_value_as_string character varying(255),
    m_value_as_number double precision,
    m_value_as_concept_id integer,
    m_value_as_datetime timestamp without time zone,
    m_qualifier_concept_id integer,
    m_unit_concept_id integer,
    m_algo_id integer NOT NULL,
    m_user_id integer NOT NULL,
    m_valid_start_datetime timestamp without time zone NOT NULL,
    m_valid_end_datetime timestamp without time zone,
    m_invalid_reason character varying(1),
    m_concept_id bigint NOT NULL
);

CREATE TABLE mapper_user (
    m_user_id integer NOT NULL,
    m_username character varying(255) NOT NULL,
    m_password character varying(255) NOT NULL,
    m_firstname character varying(255) NOT NULL,
    m_lastname character varying(255) NOT NULL,
    m_email character varying(255) NOT NULL,
    m_address character varying(255) NOT NULL,
    m_signin_datetime timestamp without time zone NOT NULL,
    m_valid_start_datetime timestamp without time zone NOT NULL,
    m_valid_end_datetime timestamp without time zone,
    m_invalid_reason character varying(1),
    m_is_admin integer
);

CREATE SEQUENCE mapper_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE mapper_user_id_seq OWNED BY mapper_user.m_user_id;

CREATE TABLE metadata (
    metadata_concept_id integer NOT NULL,
    metadata_type_concept_id integer NOT NULL,
    name character varying(250) NOT NULL,
    value_as_string text,
    value_as_concept_id integer,
    metadata_date date,
    metadata_datetime timestamp without time zone
);

CREATE TABLE relationship (
    relationship_id text NOT NULL,
    relationship_name text NOT NULL,
    is_hierarchical text NOT NULL,
    defines_ancestry text NOT NULL,
    reverse_relationship_id text NOT NULL,
    relationship_concept_id integer NOT NULL
);

CREATE TABLE source_to_concept_map (
    source_code text NOT NULL,
    source_concept_id integer NOT NULL,
    source_vocabulary_id text NOT NULL,
    source_code_description text,
    target_concept_id integer NOT NULL,
    target_vocabulary_id text NOT NULL,
    valid_start_date date NOT NULL,
    valid_end_date date NOT NULL,
    invalid_reason text
);

CREATE TABLE vocabulary (
    vocabulary_id text NOT NULL,
    vocabulary_name text,
    vocabulary_reference text,
    vocabulary_version text,
    vocabulary_concept_id bigint,
    m_project_id bigint
);

ALTER TABLE ONLY concept_relationship
    ALTER COLUMN m_concept_relationship_id SET DEFAULT nextval('concept_relationship_concept_relationship_id_seq'::regclass);

ALTER TABLE ONLY mapper_algo
    ALTER COLUMN m_algo_id SET DEFAULT nextval('mapper_algo_id_seq'::regclass);

ALTER TABLE ONLY mapper_job
    ALTER COLUMN m_job_id SET DEFAULT nextval('mapper_job_id_seq'::regclass);

ALTER TABLE ONLY mapper_user
    ALTER COLUMN m_user_id SET DEFAULT nextval('mapper_user_id_seq'::regclass);

ALTER TABLE ONLY concept_relationship
    ADD CONSTRAINT concept_relationship_pkey PRIMARY KEY (m_concept_relationship_id);

ALTER TABLE ONLY mapper_algo
    ADD CONSTRAINT pk_algo PRIMARY KEY (m_algo_id);

ALTER TABLE ONLY mapper_job
    ADD CONSTRAINT pk_mapper_job PRIMARY KEY (m_job_id);

ALTER TABLE ONLY mapper_project
    ADD CONSTRAINT pk_mapper_project PRIMARY KEY (m_project_id);

ALTER TABLE ONLY mapper_role
    ADD CONSTRAINT pk_mapper_role PRIMARY KEY (m_project_id, m_user_id, m_role_id);

ALTER TABLE ONLY mapper_user
    ADD CONSTRAINT pk_mapper_user PRIMARY KEY (m_user_id);

ALTER TABLE ONLY concept_relationship
    ADD CONSTRAINT uniq_concept_relationship UNIQUE (concept_id_1, concept_id_2, relationship_id, m_modif_start_datetime);

ALTER TABLE ONLY concept_synonym
    ADD CONSTRAINT uq_concept_synonym UNIQUE (concept_id, concept_synonym_name, language_concept_id, m_algo_id);

ALTER TABLE ONLY vocabulary
    ADD CONSTRAINT vocabulary_pkey PRIMARY KEY (vocabulary_id);

ALTER TABLE ONLY concept
    ADD CONSTRAINT xpk_concept PRIMARY KEY (concept_id);

ALTER TABLE ONLY concept_ancestor
    ADD CONSTRAINT xpk_concept_ancestor PRIMARY KEY (ancestor_concept_id, descendant_concept_id);

ALTER TABLE ONLY concept_class
    ADD CONSTRAINT xpk_concept_class PRIMARY KEY (concept_class_id);

ALTER TABLE ONLY DOMAIN
    ADD CONSTRAINT xpk_domain PRIMARY KEY (domain_id);

ALTER TABLE ONLY relationship
    ADD CONSTRAINT xpk_relationship PRIMARY KEY (relationship_id);

ALTER TABLE ONLY source_to_concept_map
    ADD CONSTRAINT xpk_source_to_concept_map PRIMARY KEY (source_vocabulary_id, target_concept_id, source_code, valid_end_date);

CREATE INDEX concept_m_project_id_idx ON concept USING btree (m_project_id);

CREATE INDEX idx_concept_ancestor_id_1 ON concept_ancestor USING btree (ancestor_concept_id);

ALTER TABLE concept_ancestor CLUSTER ON idx_concept_ancestor_id_1;

CREATE INDEX idx_concept_ancestor_id_2 ON concept_ancestor USING btree (descendant_concept_id);

CREATE UNIQUE INDEX idx_concept_class_class_id ON concept_class USING btree (concept_class_id);

ALTER TABLE concept_class CLUSTER ON idx_concept_class_class_id;

CREATE INDEX idx_concept_class_id ON concept USING btree (concept_class_id);

CREATE INDEX idx_concept_code ON concept USING btree (concept_code);

CREATE INDEX idx_concept_domain_id ON concept USING btree (domain_id);

CREATE INDEX idx_concept_relationship_id_1 ON concept_relationship USING btree (concept_id_1);

CREATE INDEX idx_concept_relationship_id_2 ON concept_relationship USING btree (concept_id_2);

CREATE INDEX idx_concept_relationship_id_3 ON concept_relationship USING btree (relationship_id);

CREATE INDEX idx_concept_relationship_modif_end ON concept_relationship USING btree (m_modif_end_datetime);

CREATE INDEX idx_concept_relationship_modif_start ON concept_relationship USING btree (m_modif_start_datetime);

CREATE INDEX idx_concept_synonym_id ON concept_synonym USING btree (concept_id);

CREATE INDEX idx_concept_vocabluary_id ON concept USING btree (vocabulary_id);

CREATE UNIQUE INDEX idx_domain_domain_id ON DOMAIN USING btree (domain_id);

ALTER TABLE DOMAIN CLUSTER ON idx_domain_domain_id;

CREATE INDEX idx_mapper_statistic_concept_id ON mapper_statistic USING btree (m_concept_id);

CREATE INDEX idx_mapper_statistic_type_id ON mapper_statistic USING btree (m_statistic_type_id);

CREATE UNIQUE INDEX idx_relationship_rel_id ON relationship USING btree (relationship_id);

ALTER TABLE relationship CLUSTER ON idx_relationship_rel_id;

CREATE INDEX idx_source_to_concept_map_code ON source_to_concept_map USING btree (source_code);

CREATE INDEX idx_source_to_concept_map_id_1 ON source_to_concept_map USING btree (source_vocabulary_id);

CREATE INDEX idx_source_to_concept_map_id_2 ON source_to_concept_map USING btree (target_vocabulary_id);

CREATE INDEX idx_source_to_concept_map_id_3 ON source_to_concept_map USING btree (target_concept_id);

ALTER TABLE source_to_concept_map CLUSTER ON idx_source_to_concept_map_id_3;

CREATE UNIQUE INDEX unique_email ON mapper_user USING btree (m_email);

CREATE UNIQUE INDEX unique_username ON mapper_user USING btree (m_username);

ALTER TABLE ONLY mapper_algo
    ADD CONSTRAINT fk_algo_user FOREIGN KEY (m_user_id) REFERENCES mapper_user (m_user_id);

ALTER TABLE ONLY mapper_role
    ADD CONSTRAINT fk_role_project FOREIGN KEY (m_project_id) REFERENCES mapper_project (m_project_id);

ALTER TABLE ONLY mapper_role
    ADD CONSTRAINT fk_role_user FOREIGN KEY (m_user_id) REFERENCES mapper_user (m_user_id);

ALTER TABLE ONLY mapper_statistic
    ADD CONSTRAINT fk_statistic_user FOREIGN KEY (m_user_id) REFERENCES mapper_user (m_user_id);

ALTER TABLE ONLY mapper_statistic
    ADD CONSTRAINT fk_statitic_algo FOREIGN KEY (m_algo_id) REFERENCES mapper_algo (m_algo_id);

--
-- FUNCTIONS
--
CREATE FUNCTION format_project (value_text text, project character varying)
    RETURNS text
    AS $BODY$
BEGIN
    IF value_text ~ ' - ' THEN
        RETURN value_text;
    END IF;
    RETURN project || ' - ' || value_text;
END
$BODY$
LANGUAGE plpgsql;

CREATE FUNCTION format_text (value_text text, sep1 text, sep2 text)
    RETURNS text
    AS $BODY$
BEGIN
    RETURN replace(replace(value_text, sep1, ''), sep2, '');
END
$BODY$
LANGUAGE plpgsql;

