import pandas as pd

from pysolrwrapper.loader_postgres.postgres_utils import PostgresClient
from pysolrwrapper.loader_postgres.susana_loader import PerimeterEnum, SusanaLoader
from pysolrwrapper.utils import resource_path_root


def test_concept_validator():
    path = str(
        resource_path_root()
        / "postgresql"
        / "loader"
        / "FHIR_data-absent-reason_concept.csv"
    )
    df = pd.read_csv(path)
    results = SusanaLoader.validate_concept(df, "FHIR - data-absent-reason")
    assert all(result.success for result in results)


def test_susana_loader_concept_only(susana_sample_con):
    pg_client = PostgresClient(susana_sample_con)
    loader = SusanaLoader(
        pg_client,
        m_project_type_id="FHIR",
        m_user_id=1,
        vocabulary_id="FHIR - data-absent-reason",
        domain_id="Procedure",
        m_language_id="EN",
        concept_path=resource_path_root()
        / "postgresql"
        / "loader"
        / "FHIR_data-absent-reason_concept.csv",
    )
    loader.run()


def test_susana_loader_concept_relationship_only(susana_sample_con):
    pg_client = PostgresClient(susana_sample_con)
    loader = SusanaLoader(
        pg_client,
        m_project_type_id="FHIR",
        m_user_id=1,
        vocabulary_id="FHIR - data-absent-reason",
        domain_id="Procedure",
        m_language_id="EN",
        relationship_path=resource_path_root()
        / "postgresql"
        / "loader"
        / "FHIR_data-absent-reason_relationship.csv",
    )
    loader.run()


def test_susana_loader_concept_and_relationhips(susana_sample_con):
    pg_client = PostgresClient(susana_sample_con)
    loader = SusanaLoader(
        pg_client,
        m_project_type_id="FHIR",
        m_user_id=1,
        vocabulary_id="FHIR - data-absent-reason",
        domain_id="Procedure",
        m_language_id="EN",
        concept_path=resource_path_root()
        / "postgresql"
        / "loader"
        / "FHIR_data-absent-reason_concept.csv",
        relationship_path=resource_path_root()
        / "postgresql"
        / "loader"
        / "FHIR_data-absent-reason_relationship.csv",
    )
    loader.run()


def test_susana_loader_refresh_concept_and_relationhips(susana_sample_con):
    pg_client = PostgresClient(susana_sample_con)
    loader = SusanaLoader(
        pg_client,
        m_project_type_id="FHIR",
        m_user_id=1,
        vocabulary_id="FHIR - data-absent-reason",
        domain_id="Procedure",
        m_language_id="EN",
        concept_path=resource_path_root()
        / "postgresql"
        / "loader"
        / "FHIR_data-absent-reason_concept.csv",
        relationship_path=resource_path_root()
        / "postgresql"
        / "loader"
        / "FHIR_data-absent-reason_relationship.csv",
    )
    loader.refresh(PerimeterEnum.ALL)
