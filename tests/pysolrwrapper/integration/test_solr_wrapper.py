from pysolrwrapper.core import SolrQuery
from pysolrwrapper.filter import FilterColumnEnum, FilterText


def test_solr_wrapped_query(solr, solr_client):
    solr_query = (
        SolrQuery(
            host="localhost",
            port=solr.get_exposed_port(8983),
            core="susana_core",
            auth=solr_client.auth,
        )
        .highlight(["concept_name"])
        .facet(["domain_id", "is_mapped"], limit=-1, missing=False, matches=["^aphp .*"])
        .weight("concept_name concept_synonym_name_en^3")
        .boost("sub(concept_id,1)")
        .add_filter(FilterColumnEnum("standard_concept", ["omop c", "omop s"]))
        .add_filter(FilterText("en", ["bob"]))
        .select(["concept_id", "score"])
        .sort("score", "desc")
        .set_type_edismax()
        .page(1)
        .limit(10)
    )
