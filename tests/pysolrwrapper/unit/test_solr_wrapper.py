from pysolrwrapper import utils

SOLR_RESULT = {
    "responseHeader": {
        "zkConnected": True,
        "status": 0,
        "QTime": 3,
        "params": {
            "facet.field": ["domain_id", "standard_concept"],
            "hl": "true",
            "fl": "*",
            "hl.requireFieldMatch": "true",
            "hl.fragsize": "10",
            "sort": "score desc",
            "termVectors": "true",
            "rows": "2",
            "hl.snippets": "10",
            "q": "(Meningeal irritation) AND standard_concept:S",
            "ps3": "2",
            "facet.limit": "5",
            "defType": "edismax",
            "qf": "concept_synonym_name^1 concept_name^2 concept_mapped_name^1",
            "sow": "false",
            "hl.method": "unified",
            "hl.fl": "concept_name,concept_synonym_name,concept_mapped_name",
            "pf3": "concept_synonym_name^3 concept_name^6 concept_mapped_name^3",
            "wt": "json",
            "facet": "true",
        },
    },
    "response": {
        "numFound": 822,
        "start": 0,
        "docs": [
            {
                "id": "441130",
                "concept_name": "Meningeal irritation",
                "domain_id": "Condition",
                "vocabulary_id": "SNOMED",
                "concept_class_id": "Clinical Finding",
                "standard_concept": "S",
                "concept_code": "70784009",
                "concept_synonym_name": [
                    "Duprés's syndrome",
                    "Meningeal irritation (disorder)",
                    "Meningismus",
                ],
                "_version_": 1621958684005367840,
            },
            {
                "id": "45885197",
                "concept_name": "Irritation",
                "domain_id": "Meas Value",
                "vocabulary_id": "LOINC",
                "concept_class_id": "Answer",
                "standard_concept": "S",
                "concept_code": "LA7449-7",
                "_version_": 1621958683628929081,
            },
        ],
    },
    "facet_counts": {
        "facet_queries": {},
        "facet_fields": {
            "domain_id": [
                "condition",
                274,
                "measurement",
                150,
            ],
            "standard_concept": ["s", 822, "c", 0],
        },
        "facet_ranges": {},
        "facet_intervals": {},
        "facet_heatmaps": {},
    },
    "highlighting": {
        "441130": {
            "concept_name": ["<em>Meningeal</em> <em>irritation</em>"],
            "concept_synonym_name": ["<em>Meningeal</em> <em>irritation</em> (disorder)"],
            "concept_mapped_name": [],
        },
        "45885197": {
            "concept_name": ["<em>Irritation</em>"],
            "concept_synonym_name": [],
            "concept_mapped_name": [],
        },
    },
}


def test_convert_basic():
    resultWanted = {
        "num_found": 822,
        "num_shown": 2,
        "docs": [
            {
                "id": "441130",
                "concept_name": "<em>Meningeal</em> <em>irritation</em>",
                "domain_id": "Condition",
                "vocabulary_id": "SNOMED",
                "concept_class_id": "Clinical Finding",
                "standard_concept": "S",
                "concept_code": "70784009",
                "concept_synonym_name": [
                    "<em>Meningeal</em> <em>irritation</em> (disorder)"
                ],
            },
            {
                "id": "45885197",
                "concept_name": "<em>Irritation</em>",
                "domain_id": "Meas Value",
                "vocabulary_id": "LOINC",
                "concept_class_id": "Answer",
                "standard_concept": "S",
                "concept_code": "LA7449-7",
            },
        ],
        "facet_fields": {
            "domain_id": [{"condition": 274}, {"measurement": 150}],
            "standard_concept": [{"s": 822}, {"c": 0}],
        },
    }
    result = utils.convert_result(SOLR_RESULT)
    assert result["num_found"] == resultWanted["num_found"]
    assert result["num_shown"] == resultWanted["num_shown"]
    assert (
        result["facet_fields"]["domain_id"] == resultWanted["facet_fields"]["domain_id"]
    )
    assert result["docs"] == resultWanted["docs"]


def test_convert_array_to_dict():
    array = ["condition", 274, "measurement", 150]
    result = utils.convert_array_to_dict(array)
    assert result == [{"condition": 274}, {"measurement": 150}]


def test_format_high():
    assert utils.format_high(["a"], ["a"]) == ["a"]
    assert utils.format_high("a", ["a"]) == "a"
