import collections
import os
from dataclasses import dataclass
from pathlib import Path

import pytest
from testcontainers.compose import DockerCompose

from app.server import app
from pysolrwrapper.loader_postgres.postgres_utils import PostgresClient, PostgresConf
from pysolrwrapper.loader_solr.pg_to_solr import pg_to_solr_query
from pysolrwrapper.loader_solr.solr_utils import SolrClient, SolrConf


@pytest.fixture(scope="session")
def path_root():
    yield Path(__file__).parent.parent


Container = collections.namedtuple("Container", ["host", "port"])


@dataclass
class Containers:
    solr: Container
    pg: Container
    solr_conf: SolrConf
    pg_conf: PostgresConf
    solr_client: SolrClient
    pg_client: PostgresClient


@pytest.fixture(scope="session")
def containers(path_root) -> Containers:
    if os.environ.get("MANUAL_MANAGE_COMPOSE", "false") == "false":
        with DockerCompose(
            str(path_root),
            compose_file_name=["docker-compose-dev.yml"],
            pull=False,
        ) as compose:
            stdout, stderr = compose.get_logs()
            if stderr:
                raise Exception("Error", stderr)

            solr = Container(
                compose.get_service_host("solr", 8983),
                compose.get_service_port("solr", 8983),
            )
            pg = Container(
                compose.get_service_host("postgres", 5432),
                compose.get_service_port("postgres", 5432),
            )

            pg_conf = PostgresConf(
                host=pg.host,
                port=pg.port,
                user="user",
                password="password",
                database="susana",
            )

            solr_conf = SolrConf(
                host=solr.host,
                port=solr.port,
                user="user",
                password="password",
                core="susana_core",
            )

            pg_client = PostgresClient.from_conf(pg_conf)
            solr_client = SolrClient(conf=solr_conf)

            tmp_file = str(path_root / "tmp" / "solr.csv")
            pg_client.bulk_export_solr(pg_to_solr_query(), tmp_file)
            solr_client.solr_bulk_load("susana_core", f"/app/tmp/solr.csv")

            yield Containers(solr, pg, solr_conf, pg_conf, solr_client, pg_client)
            Path(tmp_file).unlink()
    else:
        solr = Container("localhost", 8983)
        pg = Container(
            "localhost",
            5432,
        )

        pg_conf = PostgresConf(
            host=pg.host,
            port=pg.port,
            user="user",
            password="password",
            database="susana",
        )

        solr_conf = SolrConf(
            host=solr.host,
            port=solr.port,
            user="user",
            password="password",
            core="susana_core",
        )

        pg_client = PostgresClient.from_conf(pg_conf)
        solr_client = SolrClient(conf=solr_conf)

        tmp_file = str(path_root / "tmp" / "solr.csv")
        pg_client.bulk_export_solr(pg_to_solr_query(), tmp_file)
        solr_client.solr_bulk_load("susana_core", f"/app/tmp/solr.csv")

        yield Containers(solr, pg, solr_conf, pg_conf, solr_client, pg_client)
        Path(tmp_file).unlink()


@pytest.fixture
def client(containers):
    dsn = str(containers.pg_conf)
    os.environ["SQLALCHEMY_DATABASE_URI"] = dsn
    app.config["TESTING"] = True
    from app.db import db

    db.init_app(app)
    with app.test_client() as client:
        yield client


@pytest.fixture(scope="function", autouse=True)
def no_jwt(monkeypatch):
    """Monkeypatch the JWT verification functions for tests"""
    monkeypatch.setattr(
        "flask_jwt_extended.view_decorators.verify_jwt_in_request",
        lambda: print("Verify"),
    )
    monkeypatch.setattr("app.resources.concept.get_jwt_identity", lambda: 1)
    monkeypatch.setattr("app.resources.concept_relationship.get_jwt_identity", lambda: 1)
