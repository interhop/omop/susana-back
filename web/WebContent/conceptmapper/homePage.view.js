sap.ui.jsview("conceptmapper.homePage", {

	/**
	 * Specifies the Controller belonging to this View. In the case that it is
	 * not implemented, or that "null" is returned, this View does not have a
	 * Controller.
	 * 
	 * @memberOf conceptmapper.homePage
	 */
	getControllerName : function() {
		return "conceptmapper.homePage";
	},

	/**
	 * Is initially called once after the Controller has been instantiated. It
	 * is the place where the UI is constructed. Since the Controller is given
	 * to this method, its event handlers can be attached right away.
	 * 
	 * @memberOf conceptmapper.homePage
	 */
	createContent : function(oController) {
		// Common tiles
		var oTileImportCSV = new sap.m.StandardTile({
			title : "{i18n>tile_importCSV}",
			press:[oController.goToCSV,oController]
		});
//		var oTileAlgos = new sap.m.StandardTile({
//			title : "{i18n>tile_algos}",
//		// press:[oController.goToProductAnalytics,oController]
//		});
		var oTileSearch = new sap.m.StandardTile({
			title : "{i18n>tile_search}",
			press:[oController.goToSearch,oController]
		});
		var oTileMap = new sap.m.StandardTile({
			title : "{i18n>tile_map}",
		// press:[oController.goToProductAnalytics,oController]
		});
		
		// Specifique admin tiles
		var oTileUsers = new sap.m.StandardTile({
			title : "{i18n>tile_users}",
			press:[oController.goToUser,oController]
		});

		var oTileCont = new sap.m.TileContainer({
			tiles : [ oTileImportCSV, oTileUsers, oTileSearch,
					oTileMap ]

		});
		
		var oPage = new sap.m.Page({
			title : "{i18n>app_head}",
			enableScrolling : false,
			headerContent : [
				new sap.m.Button({
					// text:"lol", 
					icon:"sap-icon://account"
				})
			],
			content : [ oTileCont ]
		});

		return oPage;
	}

});