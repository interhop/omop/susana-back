sap.ui.jsview("conceptmapper.reviewerPage", {

	/** Specifies the Controller belonging to this View.
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf conceptmapper.reviewerPage
	*/
	getControllerName : function() {
		return "conceptmapper.reviewerPage";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
	* Since the Controller is given to this method, its event handlers can be attached right away.
	* @memberOf conceptmapper.reviewerPage
	*/
	createContent : function(oController) {


		/////////////// Master Page ///////////////

//		var oMapperTitle = new sap.m.Title({
//			width: "100%",
//			text:"{i18n>mapper_filters}",
//		});
//
//		var oItemTemplateProject = new sap.ui.core.Item({
//			key : "{m_project_type_id>key}",
//			text : "{m_project_type_id>text}",
//		});
//
//
//		var oProjectMCB = new sap.m.MultiComboBox({
//			id : "oTypeProject",
//			width: "100%",
//			placeholder : "{i18n>choose_project}",
//			items : {
//				path : "m_project_type_id>/items",
//				template : oItemTemplateProject
//			},
//			selectedKeys : {
//				path : "m_project_type_id>/selected",
//				template : "{m_project_type_id>selected}"
//			},
//			selectionFinish: [ oController.searcher, oController ],
//
//		});
//
//
//		var oItemTemplateLang = new sap.ui.core.Item({
//			key : "{m_language_id>key}",
//			text : "{m_language_id>text}",
//		});
//		var oLangMCB = new sap.m.MultiComboBox({
//			id : "oLangMCB",
//			placeholder : "{i18n>language_label}",
//			items : {
//				path : "m_language_id>/items",
//				template : oItemTemplateLang
//			},
//			selectedKeys : {
//				path : "m_language_id>/selected",
//				template : "{m_language_id>selected}"
//			},
//			selectionFinish: [ oController.searcher, oController ],
//
//		});
//
//
//		var oItemTemplateMap = new sap.ui.core.Item({
//			key : "{is_mapped>key}",
//			text : "{is_mapped>text}",
//		});
//		var oMapMCB = new sap.m.MultiComboBox({
//			id : "oMapMCB",
//			placeholder : "{i18n>mapping_label}",
//			items : {
//				path : "is_mapped>/items",
//				template : oItemTemplateMap
//			},
//			selectedKeys : {
//				path : "is_mapped>/selected",
//				template : "{is_mapped>selected}"
//			},
//
//			selectionFinish: [ oController.searcher, oController ],
//
//		});
//
//
//
//
//
//
//
//		var omop_title = new sap.m.Title({
//			width: "100%",
//			text:"{i18n>omop_filters}",
//
//
//		});
//
//		var oMiniIB = new sap.m.Input({
//			id : "oMiniIB",
//			placeholder : "{i18n>mini_id}",
//
//		});
//
//		var oMaxiIB = new sap.m.Input({
//			id : "oMaxiIB",
//			placeholder : "{i18n>maxi_id}",
//		});
//
//		var oItemTemplateStandard = new sap.ui.core.Item({
//			key : "{standard_concept>key}",
//			text : "{standard_concept>text}",
//			enabled : "{standard_concept>enabled}"
//		});
//		var oStandardMCB = new sap.m.MultiComboBox({
//			id : "oStandardMCB",
//			placeholder : "{i18n>standard_label}",
//			items : {
//				path : "standard_concept>/items",
//				template : oItemTemplateStandard
//			},
//			selectedKeys : {
//				path : "standard_concept>/selected",
//				template : "{standard_concept>selected}"
//			},
//			selectionFinish: [ oController.searcher, oController ],
//
//		});
//
//		var oItemTemplateValid = new sap.ui.core.Item({
//			key : "{invalid_reason>key}",
//			text : "{invalid_reason>text}",
//			enabled : "{invalid_reason>enabled}"
//		});
//		var oValidMCB = new sap.m.MultiComboBox({
//			id : "oValidMCB",
//			placeholder : "{i18n>valid_label}",
//			items : {
//				path : "invalid_reason>/items",
//				template : oItemTemplateValid
//			},
//			selectedKeys : {
//				path : "invalid_reason>/selected",
//				template : "{invalid_reason>selected}"
//			},
//			selectionFinish: [ oController.searcher, oController ],
//
//		});
//
//
//		var oItemTemplateDomain = new sap.ui.core.Item({
//			key : "{domain_id>key}",
//			text : "{domain_id>text}",
//			enabled : "{domain_id>enabled}"
//		});
//		var oDomainMCB = new sap.m.MultiComboBox({
//			id : "oDomainMCB",
//			placeholder : "{i18n>domain_label}",
//			items : {
//				path : "domain_id>/items",
//				template : oItemTemplateDomain
//			},
//			selectedKeys : {
//				path : "domain_id>/selected",
//				template : "{domain_id>selected}"
//			},
//			selectionFinish: [ oController.searcher, oController ],
//
//		});
//
//		var oItemTemplateVoc = new sap.ui.core.Item({
//			key : "{vocabulary_id>key}",
//			text : "{vocabulary_id>text}",
//			enabled : "{vocabulary_id>enabled}"
//		});
//		var oVocMCB = new sap.m.MultiComboBox({
//			id : "oVocMCB",
//			placeholder : "{i18n>vocabulary_label}",
//			items : {
//				path : "vocabulary_id>/items",
//				template : oItemTemplateVoc
//			},
//			selectedKeys : {
//				path : "vocabulary_id>/selected",
//				template : "{vocabulary_id>selected}"
//			},
//			selectionFinish: [ oController.searcher, oController ],
//
//		});
//
//
//
//
		var oMasterPage = new sap.m.Page({
			title : "{i18n>filters}",
			showNavButton : true,

			content : [
//				oMapperTitle,  oProjectMCB, oLangMCB, oMapMCB ,
//				omop_title, oMiniIB, oMaxiIB,  oDomainMCB, oVocMCB, oStandardMCB, oValidMCB,
//
					],


		});











		// Detail Page

		var aColumns = [
			new sap.m.Column({
				visible: false,
//
			}),
			new sap.m.Column({
				id: "conceptId1Reviewer",
				header : new sap.m.Label({
					text : "{i18n>source_id}"
				})

			}),
			new sap.m.Column({
				id: "conceptName1Reviewer",
				header : new sap.m.Label({
					text : "{i18n>source_name}"
				}),

			}),
			new sap.m.Column({
				id: "conceptDomain1Reviewer",
				header : new sap.m.Label({
					text : "{i18n>source_domain}"
				}),
				visible: false,

			}),
			new sap.m.Column({
				id: "conceptVocab1Reviewer",
				header : new sap.m.Label({
					text : "{i18n>source_vocab}"
				}),
				visible: false,
			}),


			new sap.m.Column({
				id: "conceptId2Reviewer",
				header : new sap.m.Label({
					text : "{i18n>target_id}"
				})

			}),
			new sap.m.Column({
				id: "conceptName2Reviewer",
				header : new sap.m.Label({
					text : "{i18n>target_name}"
				}),

			}),
			new sap.m.Column({
				id: "conceptDomain2Reviewer",
				header : new sap.m.Label({
					text : "{i18n>target_domain}"
				}),
				visible: false,

			}),
			new sap.m.Column({
				id: "conceptVocab2Reviewer",
				header : new sap.m.Label({
					text : "{i18n>target_vocab}"
				}),
				visible: false,
			}),

			new sap.m.Column({
				id: "usernameReviewer",
				header : new sap.m.Label({
					text : "{i18n>username}"
				}),

			}),
			new sap.m.Column({
				id: "voteReviewer",
				header : new sap.m.Text({
					text : "{i18n>vote}"
				}),
				visible: true,
			}),

			new sap.m.Column({
				id: "upReviewer",
				header : new sap.m.Text({
					text : "{i18n>up}"
				}),
			}),


			new sap.m.Column({
				id: "downReviewer",
				header : new sap.m.Text({
					text : "{i18n>down}"
				}),
			}),


		];








		var selectedItems = []

		var oTemplate = new sap.m.ColumnListItem({

			type: "Inactive",
//			highlight:"{conceptsModel>status}",
//			press : [ oController.goToConceptDetail, oController ],

			cells : [

				new sap.m.Text({
					text : "{relationshipsModel>m_concept_relationship_id}",

				}),

				new HoverLink({
					text : "{relationshipsModel>concept_id_1}",
					wrapping : false,
                	press: [  oController.goToConceptDetail, oController ],

                	mouseOver: [  oController.showDetailPopover, oController ],
                	mouseOut: [  oController.hideDetailPopover, oController ],


				}),
				new sap.m.Text({
					text : "{relationshipsModel>concept_name_1}",
					wrapping : true,
				}),
				new sap.m.Text({
					text : "{relationshipsModel>domain_id_1}",
					wrapping : false,

				}),
				new sap.m.Text({
					text : "{relationshipsModel>vocabulary_id_1}",
					wrapping : false,

				}),



				new HoverLink({
					text : "{relationshipsModel>concept_id_2}",
					wrapping : false,
                	press: [  oController.goToConceptDetail, oController ],



                	mouseOver: [  oController.showDetailPopover, oController ],
                	mouseOut: [  oController.hideDetailPopover, oController ],


				}),
				new sap.m.Text({
					text : "{relationshipsModel>concept_name_2}",
					wrapping : true,
				}),
				new sap.m.Text({
					text : "{relationshipsModel>domain_id_2}",
					wrapping : false,

				}),
				new sap.m.Text({
					text : "{relationshipsModel>vocabulary_id_2}",
					wrapping : false,

				}),



				new sap.m.Text({
					text : "{relationshipsModel>m_username}",
					wrapping : false,

				}),
				new sap.m.Text({
					text : "{relationshipsModel>vote}",
					wrapping : false,

				}),

				new sap.m.Button({
					icon: "sap-icon://thumb-up",
//                	tooltip: "{i18n>filters}",
					type: "{relationshipsModel>type_up}",
					enabled:true,
	                press:  [ "UP", oController.vote, oController ],

                }),

                new sap.m.Button({
					icon: "sap-icon://thumb-down",
//                	tooltip: "{i18n>filters}",
					type: "{relationshipsModel>type_down}",
					enabled:true,
	                press:  [ "DOWN", oController.vote, oController ],
                }),


			]
		});


		var oTable = new sap.m.Table({
			id : "oReviewTable",
			fixedLayout:false,

			columns : aColumns,

		});




		oTable.bindItems({
			path : "relationshipsModel>/relations",

			template : oTemplate,
		});























//
//
//		var oSubHeader = new sap.m.Bar({
//			contentMiddle : [ new sap.m.Label({
//				text : "{i18n>app_subhead_search}"
//			}),
//			new sap.m.Button({
//				icon: "sap-icon://sort"
//			})
//			]
//		});




		var oToolbarConceptsNumber = new sap.m.Toolbar({
			height: "4rem",
			content : [

				new sap.m.ToolbarSpacer(),
				new sap.m.Label({
					id:"numFoundReviewerPage",
					text:"{relationshipsModel>/num_found}",
//					visible:false
				}).addStyleClass('conceptNumFound'),
				new sap.m.Button({
					text:"{i18n>concepts_found}",
					enabled:false,
					type: sap.m.ButtonType.Transparent,
				}),

			]
		}).addStyleClass('borderBottom');


		var oRowsPerLignTemplate = new sap.ui.core.Item({
			key : "{RowsPerLignModel>key}",
			text : "{RowsPerLignModel>text}",
			enabled : "{RowsPerLignModel>enabled}"
		});


		var oItemTemplateProject = new sap.ui.core.Item({
			key : "{m_project_type_id_reviewer>key}",
			text : "{m_project_type_id_reviewer>text}",
		});
//
		var oToolbarControls = new sap.m.Toolbar({
			content : [




//                new sap.m.Button({
//                	id: "hideShowFilterReviewer",
//					icon: "sap-icon://filter",
////                	tooltip: "{i18n>filters}",
//					type: "Transparent",
//					enabled:true,
//					type: "Transparent",
//                	press: function(){
//     		    	    sap.ui.getCore().byId("__container2-Master").toggleStyleClass("toggle-masterPage");
//                	}
//                }),
//




    		new sap.m.MultiComboBox({
    			id : "oTypeProjectReviewer",
    			width: "10%",
    			placeholder : "{i18n>choose_project}",
    			items : {
    				path : "m_project_type_id_reviewer>/items",
    				template : oItemTemplateProject
    			},
    			selectedKeys : {
    				path : "m_project_type_id_reviewer>/selected",
    				template : "{m_project_type_id_reviewer>selected}"
    			},
    			selectionFinish: [ oController.searchRelation, oController ],

    		}),
//



//				new sap.m.Button({
//                	id: "sortSearchPage",
//					icon: "sap-icon://sort",
//					type: "Transparent",
//					enabled:false,
//					press: [oController.openSortSPFragment, oController],
//				}),

//				new sap.m.Button({
//                	id: "customizeReviewerPage",
//					icon: "sap-icon://customize",
//                	tooltip: "{i18n>select_columns}",
//					type: "Transparent",
////                	enabled:false,
////					press: [oController.openCustomizeSPFragment, oController],
//				}),
				new sap.m.ToolbarSpacer(),


//				new sap.m.SearchField({
//					id: 'searchField',
//					placeholder : "{i18n>research}",
//					enabled:false,
//					search : [ oController.searcher, oController ],
//				}).addStyleClass('searchField'),
//				new sap.m.ToolbarSpacer(),


				new sap.m.ComboBox({
					id: "oRowInPageReviewerPage",
//					enabled:false,
					items : {
						path : "RowsPerLignModel>/items",
						template : oRowsPerLignTemplate
					},
					selectedKey : {
						path : "RowsPerLignModel>/selected",
						template : "{RowsPerLignModel>selected}"
					},
					change :  [ oController.searchRelation, oController ],

				}).addStyleClass('rowInPageInput'),
				new sap.m.Button({
					text : "{i18n>config_items_per_page}",
					enabled: false,
					type: sap.m.ButtonType.Transparent,

				}),

			]
		});



		var oDetailPage = new sap.m.Page({
//			title : "{i18n>firstname}",
			showNavButton : false,
			navButtonPress : function(oEvt) {
				app.back();
			},
			customHeader : new sap.m.Bar({
                contentLeft: new sap.m.Button({
					icon : "sap-icon://home-share",
					text: "Git",
					press: function() {
						window.open('https://interhop.frama.io/omop/susana/');
					}
				}),
				contentMiddle: new sap.m.Title({
					text: "{i18n>mapping_reviewer}",
				}),
                contentRight: [

                	new sap.m.Button({
    					id: 'usersParamReviewer',
    					icon : "sap-icon://collaborate",
    					visible: false,
    					press: [ oController.goToUserPage, oController ],

    				}),

					new sap.m.Button({
						text: '{loginModel>/m_username}',
						icon : "sap-icon://employee",
						press: [ oController.userInfos, oController ],
					}),
					new sap.m.Button({
						text: 'Log-out',
						icon : "sap-icon://decline",

						press: [ oController.logout, oController ],
				})
			]}),
			content : [oToolbarConceptsNumber, oToolbarControls,  oTable ],

			footer: new sap.m.Bar({



        		contentMiddle: [



            		new sap.m.SearchField({
    					id: 'oSearchFielGoToPageReviewer',
//    					enabled:false,
            			placeholder : "{i18n>go_to_page}",
    					search : [ oController.searcherGoToPage, oController ],
    				}).addStyleClass('goTo'),

        			new sap.m.Button({
            			id : "oButtonCurrentPageReviewer",
                        text: "{i18n>current_page}" ,
                        enabled:false,
                	}),
        			new sap.m.Button({
	            		id : "oButtonPrevReviewer",
	                	text: "{i18n>previous}",
						type : "Transparent",
//						enabled: false,
						icon : "sap-icon://navigation-left-arrow",
    					press : [ oController.prevPage, oController ],

                	}),
                	new sap.m.Button({
	            		id : "oButtonNextReviewer",
	                	text: "{i18n>next}",
    					type : "Transparent",
//						enabled: false,
    					iconFirst: false,
    					icon : "sap-icon://navigation-right-arrow",
    					press : [ oController.nextPage, oController ],
                	}),
	                new sap.m.Button({
            			id : "oButtonTotalPagesReviewer",
	                    text: "{i18n>total_pages}",
	                    enabled:false,
	            	}),

                ],

                contentRight: [



                	 new sap.m.Button({
             			id : "switchToMapperApp",
    					type : "Emphasized",
    					icon : "sap-icon://journey-change",
                    	tooltip: "{i18n>switch_to_mapper_app}",
// 	                    enabled:false,
 	                    press:  [ oController.goToSearchPage, oController ]

 	            	}),


                ]



			})
		}).addStyleClass('page');



		/////////////// All in one Page ///////////////

		var oSplitContainer = new sap.m.SplitContainer({mode: sap.m.SplitAppMode.ShowHideMode});
		oSplitContainer.addMasterPage(oMasterPage);
		oSplitContainer.addDetailPage(oDetailPage);


		return oSplitContainer;
	}

});
