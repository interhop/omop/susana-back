var oItemTemplateAdmin = new sap.ui.core.Item({
	key : "{isAdminUserModel>key}",
	text : "{isAdminUserModel>text}",
	enabled : "{isAdminUserModel>enabled}"
});

var oItemTemplateValid = new sap.ui.core.Item({
	key : "{isValidUserModel>key}",
	text : "{isValidUserModel>text}",
	enabled : "{isValidUserModel>enabled}"
});





sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./utils/userAjax",
	"./utils/usersAjax"
		], function(Controller, userAjax, usersAjax) {
		   "use strict";

return Controller.extend("conceptmapper.userPage", {

	
	/******* CREATE *******/
	openCreateDialog : function (oEvt) {

		var oCreateDialog = new sap.m.Dialog({
			title : "{i18n>create_user}",
			icon : "sap-icon://employee"
		});	
		

		var oSimpleForm = new sap.ui.layout.form.SimpleForm({
			content : [ new sap.m.Title({
				text : "Person"
			}), new sap.m.Label({
				text : "{i18n>username}" 
			}), new sap.ui.commons.TextField({
				value : ""
			}), new sap.m.Label({
				text : "{i18n>status}" 
			}), new sap.m.ComboBox({

				items : {
					path : "isValidUserModel>/items",
					template : oItemTemplateValid
				},
				selectedKeys : {
					path : "isValidUserModel>/selected",
					template : "{isValidUserModel>selected}"
				},
			}), new sap.m.Label({
				text : "{i18n>is_admin}" 
			}), new sap.m.ComboBox({

				items : {
					path : "isAdminUserModel>/items",
					template : oItemTemplateAdmin
				},
				selectedKeys : {
					path : "isAdminUserModel>/selected",
					template : "{isAdminUserModel>selected}"
				},
			}), new sap.m.Label({
				text : "{i18n>password}" 
			}), new sap.ui.commons.TextField({
				value : ""
			}), new sap.m.Label({
				text : "{i18n>firstname}" 
			}), new sap.ui.commons.TextField({
				value : ""
			}), new sap.m.Label({
				text : "{i18n>lastname}" 
			}), new sap.ui.commons.TextField({
				value : ""
			}), new sap.m.Label({
				text : "{i18n>address}" 
			}), new sap.ui.commons.TextField({
				value : ""
			}), new sap.m.Label({
				text : "{i18n>email}" 
			}), new sap.ui.commons.TextField({
				value : ""
			}), 
			]
		});

		oCreateDialog.addContent(oSimpleForm);

		oCreateDialog.addButton(
                new sap.m.Button({
                    text: "Submit",
					icon: "sap-icon://accept",
					
                    press: function () {
                    	
                    
                    	
                        var content = oSimpleForm.getContent();
             
                        var oData = {};
                        oData.m_username = content[2].getValue();
                        oData.m_invalid_reason = (content[4].getValue() === 'Valid') ? null : 'D';
                        oData.m_is_admin = content[6].getValue();
                        oData.m_password = content[8].getValue();
                        oData.m_firstname = content[10].getValue();
                        oData.m_lastname = content[12].getValue();
                        oData.m_address = content[14].getValue();
                        oData.m_email = content[16].getValue();

                        
                        if (oData.m_username !== ""  && oData.m_password !== "" 
                        	&& oData.m_firstname !== "" && oData.m_lastname !== "" 
                        		&& oData.m_address !== "" && oData.m_email !== ""){
                         
                        	userAjax.postUserAjax(oData, content[2].getValue());
                        	usersAjax.getUsers();                       	
                            
                        } else {
                        	sap.m.MessageToast.show("Nothing created!! \nYou have to file all the fields");
                        }
						oCreateDialog.close();
					}
                })
        );
		oCreateDialog.addButton(
				new sap.m.Button({
					text: "Reject",
					icon: "sap-icon://decline",
					press: function () {
						oCreateDialog.close();
					}
				})
        );
		
		oCreateDialog.open();
	},
	
	
	
	
	/******* UPDATE *******/

	openUpdateDialog : function(oEvt) {



		var button_press = oEvt.getParameters().id;
		var oTable_ref = sap.ui.getCore().byId(button_press).getParent().getParent();
		var idx = oTable_ref.getSelectedIndices();
        if (idx.length === 0) {
        	sap.m.MessageToast.show("Nobody to update");
        	return;
        } else if (idx.length > 1) {
        	sap.m.MessageToast.show("You can update one user each time");
    		return;
        }
        var user = oTable_ref.getRows()[idx[0]].getCells();
        
        var oUpdateDialog = new sap.m.Dialog({
			title : "{i18n>update_user}",
			icon : "sap-icon://employee"
		});
        
        // get the model called user to get the password
        userAjax.getUser(oTable_ref.getRows()[idx[0]].getCells()[0].getProperty('text'));
        
		var oSimpleForm = new sap.ui.layout.form.SimpleForm({
			content : [ new sap.m.Title({
				text : "Person"
			}), new sap.m.Label({
				text : "{i18n>username}" 
			}), new sap.ui.commons.TextField({
				enabled: false,
				value : "{userModel>/m_username}"
			}), new sap.m.Label({
				text : "{i18n>status}" 
			}), new sap.m.ComboBox({
				items : {
					path : "isValidUserModel>/items",
					template : oItemTemplateValid
				},
				selectedKey : "{userModel>/m_invalid_reason}"
			}), new sap.m.Label({
				text : "{i18n>is_admin}" 
			}), new sap.m.ComboBox({
	
				items : {
					path : "isAdminUserModel>/items",
					template : oItemTemplateAdmin
				},
				selectedKey : "{userModel>/m_is_admin}"
			}), new sap.m.Label({
				text : "{i18n>password}" 
			}), new sap.ui.commons.TextField({
				value : "{userModel>/m_password}"
			}), new sap.m.Label({
				text : "{i18n>firstname}" 
			}), new sap.ui.commons.TextField({
				value : "{userModel>/m_firstname}"
			}), new sap.m.Label({
				text : "{i18n>lastname}" 
			}), new sap.ui.commons.TextField({
				value : "{userModel>/m_lastname}"
			}), new sap.m.Label({
				text : "{i18n>address}" 
			}), new sap.ui.commons.TextField({
				value : "{userModel>/m_address}"
			}), new sap.m.Label({
				text : "{i18n>email}" 
			}), new sap.ui.commons.TextField({
				value : "{userModel>/m_email}"
			}), 
			]
		});

		oUpdateDialog.addContent(oSimpleForm);
		
		oUpdateDialog.addButton(
                new sap.m.Button({
                    text: "Submit",
					icon: "sap-icon://accept",
					
                    press: function () {
                        var content = oSimpleForm.getContent();
                        var oData = {};
                        
                        
                        //oData.m_username = content[2].getValue();
                        oData.m_invalid_reason = (content[4].getValue() === 'Valid') ? null : 'D';
                        oData.m_is_admin = content[6].getValue();
                        if (content[8].getValue() !== "") {
                            oData.m_password = content[8].getValue();
                        }
                        oData.m_firstname = content[10].getValue();
                        oData.m_lastname = content[12].getValue();
                        oData.m_address = content[14].getValue();
                        oData.m_email = content[16].getValue();
                        
                        if (oData.m_username !== "" 
                        	&& oData.m_firstname !== "" && oData.m_lastname !== "" 
                        		&& oData.m_address !== "" && oData.m_email !== ""){

	 
                        	userAjax.updateUserAjax(oData, content[2].getValue());
                        	usersAjax.getUsers();
                        	
		                      
                        } else {
                        	sap.m.MessageToast.show("Nothing created!! \nYou have to file all the fields");
                        }
                        
                        
                        
          
                    	oUpdateDialog.close();
					}
                })
        );
		oUpdateDialog.addButton(
				new sap.m.Button({
					text: "Reject",
					icon: "sap-icon://decline",
					press: function () {
						oUpdateDialog.close();
					}
				})
        );
		oUpdateDialog.open();

	},

	
	
	changeProject : function(oEvt) {
		var sUsernameToChange = oEvt.getSource().getParent().getAggregation('cells')[0].getProperty('text');
		
		var oRolesToChange_ref = oEvt.getSource().getSelectedItems();
		var oRolesToChange_list = [];
		for (var i in oRolesToChange_ref) {
			oRolesToChange_list.push(oRolesToChange_ref[i].getProperty('key'));
		}
		debugger;
		var oData = {};
//		oData.m_project_role = JSON.stringify(oRolesToChange_list);
		oData.m_project_role = oRolesToChange_list;
		
		userAjax.changeProjectsAjax(oData, sUsernameToChange);
		
	},
	
	
	
	
/**
 * Called when a controller is instantiated and its View controls (if
 * available) are already created. Can be used to modify the View before it
 * is displayed, to bind event handlers and do other one-time
 * initialization.
 * 
 * @memberOf conceptmapper.userPage
 */
//onInit : function() {
//	
//},
	
/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's
 * View is re-rendered (NOT before the first rendering! onInit() is used for
 * that one!).
 * 
 * @memberOf conceptmapper.userPage
 */
 onBeforeRendering: function() {
	 
		// initialisation model for admin template = model "isAdminModel"
		var oModel = new sap.ui.model.json.JSONModel();
		var mData = {
			"selected" : [],
			"items" : [{
				"key" : "Not-Admin",
				"text" : "Not-Admin"
			},
			{
				"key" : "Admin",
				"text" : "Admin"
			}]
		};
		oModel.setData(mData);
		sap.ui.getCore().setModel(oModel, "isAdminUserModel");
	 
		// users initialisation = model "usersModel"
		
		var oModel = new sap.ui.model.json.JSONModel();
		var mData = {
			"selected" : [],
			"items" : [{
				"key" : "Valid",
				"text" : "Valid"
			},
			{
				"key" : "Invalid",
				"text" : "Invalid"
			}]
		};
		oModel.setData(mData);
		sap.ui.getCore().setModel(oModel, "isValidUserModel");

 },
/**
 * Called when the View has been rendered (so its HTML is part of the document).
 * Post-rendering manipulations of the HTML could be done here. This hook is the
 * same one that SAPUI5 controls get after being rendered.
 * 
 * @memberOf conceptmapper.userPage
 */
// onAfterRendering: function() {
//
// },
/**
 * Called when the Controller is destroyed. Use this one to free resources and
 * finalize activities.
 * 
 * @memberOf conceptmapper.userPage
 */
// onExit: function() {
//
// }
});
});