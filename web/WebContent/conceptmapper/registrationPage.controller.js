sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./utils/userAjax"
		], function(Controller, userAjax) {
		   "use strict";

return Controller.extend("conceptmapper.registrationPage", {
	
	registration : function (oEvt) {
		var oData = {};
		
		var sUsername = sap.ui.getCore().byId('username').getProperty('value');
		oData.m_firstname = sap.ui.getCore().byId('firstname').getProperty('value');
		oData.m_lastname = sap.ui.getCore().byId('lastname').getProperty('value');
		oData.m_email = sap.ui.getCore().byId('email').getProperty('value');
		oData.m_address = sap.ui.getCore().byId('address').getProperty('value');
		oData.m_password = sap.ui.getCore().byId('password').getProperty('value');
		oData.m_password = sap.ui.getCore().byId('password').getProperty('value');
		var confimPassword = sap.ui.getCore().byId('confirmPassword').getProperty('value');

		
		
		// confirm pwd
		
		if (sUsername !== "" && oData.m_firstname !== "" && oData.m_lastname !== ""  
			&& oData.m_address !== ""  && oData.m_email !== "" && oData.m_password !== ""  
				&& oData.m_password === confimPassword) {
			userAjax.postUserAjax(oData, sUsername);
			app.back();
			
		} else if (oData.m_password !== confimPassword) {
			sap.m.MessageToast.show("Not the same password");
			console.log(error);
		} else {
			sap.m.MessageToast.show("You have to fill all the fields");
			console.log(error);
		}
		
	}

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf conceptmapper.registrationPage
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf conceptmapper.registrationPage
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf conceptmapper.registrationPage
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf conceptmapper.registrationPage
*/
//	onExit: function() {
//
//	}

});
});