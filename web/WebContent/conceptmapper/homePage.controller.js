sap.ui.controller("conceptmapper.homePage", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf conceptmapper.homePage
*/
//	onInit : function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf conceptmapper.homePage
*/
//	onBeforeRendering: function() {
//
//	},

	//This will navigate the app to product search page
	goToSearch: function(oEvt){
		app.to("idSearchPage");
		
	},
	
	goToUser: function(oEvt){
		app.to("idUserPage");
		
	},
	
	goToCSV: function(oEvt){
		app.to("idCSVPage");
		
	},
/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf conceptmapper.homePage
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf conceptmapper.homePage
*/
//	onExit: function() {
//
//	}

});