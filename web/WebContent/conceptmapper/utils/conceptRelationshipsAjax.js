sap.ui.define([], function() {
   "use strict";

   return {
  
	   
	   

	   getConceptRelationshipsAjax : function (oData) {
	    	sap.ui.core.BusyIndicator.show(0);

	    	var URL_relationships = "http://localhost:5000/relationships";

		   	if (sap.ui.getCore().getModel('loginModel') !== undefined) {
		   		var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
		   		var m_user_id = sap.ui.getCore().getModel('loginModel').oData.m_user_id;
		   	}
		    	
		   	$.ajax({
		   		type:"GET",
		   		url: URL_relationships,
		   		dataType:"json",
		   		data: {"json" : JSON.stringify(oData)},
		   		async: false,
		   		header: {
		   			"Content-Type": "application/json",
		   		},
		   		beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
		   		success: function(data) {
		   			debugger;
	
		   			for (var i = 0; i < data.relations.length; i++) {
		   				var relation = data.relations[i];
		   				debugger;
	
		   				relation.type_down = sap.m.ButtonType.Transparent;
		   				relation.type_up = sap.m.ButtonType.Transparent;
	
		   				for (var j in relation.users_down) {
		   					debugger;
	
		   					if (m_user_id === relation.users_down[j]) {
		   						relation.type_down = sap.m.ButtonType.Reject;
		   					}
		   				}
		   				for (var k in relation.users_up) {
		   					debugger;
	
		   					if (m_user_id === relation.users_up[k]) {
		   						relation.type_up = sap.m.ButtonType.Accept;
		   					}
		   				}
		   			}
	
		   			
	
		   			
		   			var oModel = new sap.ui.model.json.JSONModel(data);
		   			sap.ui.getCore().setModel(oModel, "relationshipsModel");
		   			
		   			console.log("Connection : getRelationshipsAjax()");
		   		},
		   		error : function(error) {
		   			sap.m.MessageToast.show("Connexion error");
		   			console.log("Connexion error : getRelationshipsAjax()");
		   			console.log(error);
		   		}
		   		
		   	});
		   	
	    	sap.ui.core.BusyIndicator.hide();

	   }

	   
	   
   };
});