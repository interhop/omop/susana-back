sap.ui.define([], function() {
   "use strict";

   return {
	   
	   
	   setAvailable : function (sVal) {

    		            			
    		            			var URL_concepts = "http://localhost:5000/projects";

//    		            		   	if (sap.ui.getCore().getModel('loginModel') !== undefined) {
//    		            		   		var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
//    		            		   	}
    		            		   	
    		            			
    		            		   	$.ajax({
    		            		   		type:"GET",
    		            		   		url:URL_concepts,
    		            		   		dataType:"json",
    		            		   		async: false,
    		            		   		header: {
    		            		   			"Content-Type": "application/json",
    		            		   		},
//    		            		   		beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
    		            		   		success: function(data) {
    		            	
    		            		   			var items = [];
    		            		   			
    		            		   			for (var idx in data.projects_roles_name) {
    		            		   				
    		            		   					items.push({
    		            		   						"key": data.projects_roles_name[idx],
    		            		   						"text": data.projects_roles_name[idx],
    		            		   					});	
    		            	//	   				}
    		            	
    		            		   			}
    		            		   			var oModel = new sap.ui.model.json.JSONModel();
    		            		   			var mData = {
    		            		   				"items" : items
    		            		   			};
    		            		   			oModel.setData(mData);
    		            		   			sap.ui.getCore().setModel(oModel, 'availableProjects');
    		            		   			
    		            		   		},
    		            	   			error : function(error) {		
    		            		   			sap.m.MessageToast.show("Connexion error");
    		            		   			console.log("Connexion error : availableProjects()");
    		            		   			console.log(error);
    		            	
    		            		   		}
    		            		   	});
	   }
   }});