sap.ui.jsfragment("conceptmapper.conceptDetail", {
	
	createContent: function(oController) {

		
		
		
		var oPopover = new sap.m.Popover({
//			title : "{i18n>select_columns}",
			showHeader:false,
			horizontalScrolling:false,
			content : new sap.m.ObjectListItem({
				title : "{conceptDetailModel>/concept_name}",
				number : "{conceptDetailModel>/standard_concept}",
				numberUnit : "{conceptDetailModel>/invalid_reason}",
				attributes : [ 
					new sap.m.ObjectAttribute({
					title : "{i18n>concept_id}",
					text : "{conceptDetailModel>/concept_id}",
				}), new sap.m.ObjectAttribute({
					title : "{i18n>concept_code}",
					text : "{conceptDetailModel>/concept_code}",

				}), new sap.m.ObjectAttribute({
					title : "{i18n>concept_class_id}",
					text : "{conceptDetailModel>/concept_class_id}",
				}), new sap.m.ObjectAttribute({
					title : "{i18n>vocabulary_id}",
					text : "{conceptDetailModel>/vocabulary_id}",
				}), new sap.m.ObjectAttribute({
					title : "{i18n>domain_id}",
					text : "{conceptDetailModel>/domain_id}",
				}), new sap.m.ObjectAttribute({
					title : "{i18n>synonyms}",
					text: "{conceptDetailModel>/synonyms}",
				}), 
				]
			}),
			
		});
		
		
		
		
		return oPopover;
		
	}
});