window.ctrlDown = false;

function showPopUp(thiss) {
    
    $(document).keydown(function(evt){
		
		if ( (evt.ctrlKey)) { // backspace
	       window.ctrlDown = true;
	    }

    });
    
    $(document).keyup(function(evt){
       window.ctrlDown = false;
    });
	
	if (window.ctrlDown) {
		thiss.fireMouseOver();

	}
}


sap.ui.define([
	"sap/m/Link",

], 


function (Link) {
	
	"use strict";
	
	// doc = https://help.sap.com/saphelp_uiaddon10/helpdata/en/d5/b756bf4e9a4d67961fa21e1ba12c9e/content.htm?no_cache=true
	
	return Link.extend("HoverLink", { // call the new Control type "HoverLink"  and let it inherit from sap.m.Link
		
		metadata: {
			events: {
				"mouseOver" : {},  // this Button has also a "hover" event, in addition to "press" of the normal Button
				"mouseOut" : {} ,
	          }
	      },
	  
	      onmouseover : function(evt) {   // is called when the Button is hovered - no event registration required
	    	  debugger;	
	    	  
	    	  showPopUp(this);
			   
	      },
	      
	      onmouseout : function(evt) {   
	    	  debugger;
	          this.fireMouseOut();
	      },

	      renderer: {} // add nothing, just inherit the LinkRenderer as is; (since the renderer is not changed)
	  });
});