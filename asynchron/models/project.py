from datetime import datetime, timedelta

from sqlalchemy import TIMESTAMP, Column, String, Text

from asynchron.db import Base, session


class ProjectModel(Base):
    __tablename__ = "mapper_project"
    __table_args__ = {"schema": "susana"}

    m_project_id = Column(
        Text(),
        primary_key=True,
    )
    m_project_type_id = Column(String(255))
    m_valid_start_datetime = Column(TIMESTAMP(), default=datetime.now())
    m_valid_end_datetime = Column(TIMESTAMP(), default=datetime.now() + timedelta(weeks=5200))
    m_invalid_reason = Column(String(1), nullable=False)

    def __init__(
        self,
        m_project_id,
        m_project_type_id,
        m_valid_end_datetime=None,
        m_invalid_reason=None,
    ):
        self.m_project_id = m_project_id
        self.m_project_type_id = m_project_type_id
        if m_valid_end_datetime != None:
            self.m_valid_end_datetime = m_valid_end_datetime
        self.m_invalid_reason = m_invalid_reason

    def upserting(self):
        session.add(self)
        session.commit()

    def delete_from_db(self):
        session.delete(self)
        session.commit()

    @classmethod
    def find_by_id(cls, m_project_id):
        return session.query(cls).filter_by(m_project_id=m_project_id).first()

    @classmethod
    def find_all(cls):
        return session.query(cls).all()
