from sqlalchemy import Column, Integer, String, and_

from asynchron.db import Base, session


class JobModel(Base):
    __tablename__ = "mapper_job"
    __table_args__ = {"schema": "susana"}

    m_job_id = Column(
        Integer,
        primary_key=True,
    )

    m_concept_id = Column(Integer, nullable=False)
    m_job_type_id = Column(String, nullable=False)
    m_user_id = Column(Integer, nullable=False)
    m_status_id = Column(Integer, nullable=False)

    def __init__(self, m_concept_id, m_job_type_id, m_user_id, m_status_id=0):
        self.m_concept_id = m_concept_id
        self.m_job_type_id = m_job_type_id
        self.m_status_id = m_status_id
        self.m_user_id = m_user_id

    def upserting(self):
        session.add(self)
        session.commit()

    def delete_from_db(self):
        session.delete(self)
        session.commit()

    @classmethod
    def find_by_initial_status(cls, initial_status, m_job_type_id):
        return (
            session.query(cls)
            .filter(
                and_(
                    cls.m_status_id.in_(initial_status),
                    cls.m_job_type_id.in_(m_job_type_id),
                )
            )
            .all()
        )

    @classmethod
    def find_by_id(cls, m_concept_id, m_job_type_id):
        return (
            session.query(cls)
            .filter(
                and_(
                    cls.m_concept_id.in_(m_concept_id),
                    cls.m_job_type_id.in_(m_job_type_id),
                )
            )
            .all()
        )

    @classmethod
    def jobs_to_process(cls, jobs_filter):
        # check if spark jobs to do and change status
        jobs = cls.find_by_initial_status([0, 1, 3], jobs_filter)
        for job in jobs:
            job.m_status_id = 1
            job.upserting()

        return jobs

    @classmethod
    def update_mapper_job(cls, dest_status, initial_status=[], job_type=[], _id=-1):
        """
        possible to search with id or initial_status
        """
        if _id == -1:
            jobs = JobModel.find_by_initial_status(initial_status, job_type)
        else:
            jobs = JobModel.find_by_id([_id], job_type)

        for job in jobs:
            job.m_status_id = dest_status
            job.upserting()

        jobs = [(job.m_concept_id, job.m_user_id) for job in jobs]
        return jobs


if __name__ == "__main__":
    list_id = []
    for id in list_id:
        newJob = JobModel(id, "SYNONYM_TRANSLATION_API_EXTEND", 1)
        newJob.upserting()
