from sqlalchemy import Column, Integer, Text

from asynchron.db import Base, session


class VocabularyModel(Base):
    __tablename__ = "vocabulary"
    __table_args__ = {"schema": "susana"}

    vocabulary_id = Column(
        Text(),
        primary_key=True,
    )
    vocabulary_name = Column(Text())
    vocabulary_reference = Column(Text())
    vocabulary_version = Column(Text())
    vocabulary_concept_id = Column(Integer, nullable=False)
    m_project_id = Column(Integer, nullable=False)

    def __init__(
        self,
        vocabulary_id,
        vocabulary_name,
        vocabulary_reference,
        vocabulary_version,
        vocabulary_concept_id,
        m_project_id,
    ):
        self.vocabulary_id = vocabulary_id
        self.vocabulary_name = vocabulary_name
        self.vocabulary_reference = vocabulary_reference
        self.vocabulary_version = vocabulary_version
        self.vocabulary_concept_id = vocabulary_concept_id
        self.m_project_id = m_project_id

    def upserting(self):
        session.add(self)
        session.commit()

    def delete_from_db(self):
        session.delete(self)
        session.commit()

    @classmethod
    def find_by_id(cls, vocabulary_id):
        return session.query(cls).filter_by(vocabulary_id=vocabulary_id).first()

    @classmethod
    def find_by_concept_id(cls, vocabulary_concept_id):
        return (
            session.query(cls)
            .filter_by(vocabulary_concept_id=vocabulary_concept_id)
            .first()
        )

    @classmethod
    def find_distinct_vocabulary_id(cls):
        return session.query(cls).distinct(cls.vocabulary_id).all()
