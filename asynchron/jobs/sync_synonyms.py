from asynchron.models.concept import ConceptModel
from asynchron.models.job import JobModel
from asynchron.models.project import ProjectModel
from asynchron.models.synonym import SynonymModel
from share.macros import ISO_OMOP_LANGUAGES, SEPARATOR, Algo


# synonyms catcher (for one id)
def sync_synonyms_between_projects():
    # take synonymfrom translate API or from_other_project_trans
    # base from on concept_id
    # => search relative_concept_id ie same vocabulary_id ('APHP - ccam' and 'LILLE - ccam') and same concept_code
    # => check if synonyms for the concept
    # => catch synonym for this concept_id

    result = JobModel.update_mapper_job(
        initial_status=[0, 1, 3],
        dest_status=1,
        job_type=["EXTEND_SYNONYMS_WITH_OTHER_PROJECT"],
    )
    print(len(result))

    if len(result) > 0:
        for _id, m_user_id in result:

            print("concepts: ", _id, ConceptModel.find_by_id(_id).concept_name)

            # find concept_id with JOIN on voc and concept_code
            relative_concepts = ConceptModel.find_concept_in_similar_voc(
                _id
            )  # return list
            relative_ids = [c.concept_id for c in relative_concepts]

            if len(relative_ids) > 0:

                # get all the synonym for the concept_id tested (API_translate + if other_project)
                synonyms = SynonymModel.find_synonym(
                    _id,
                    m_algo_id=[
                        Algo.AUTOMATIC_TRANS.value,
                        Algo.FROM_OTHER_PROJECTS_TRANS.value,
                    ],
                )
                synonyms_language_concept_id = [s.language_concept_id for s in synonyms]
                print("synonyms_language_concept_id", synonyms_language_concept_id)

                for _, omop_concept_id in ISO_OMOP_LANGUAGES.items():
                    if (
                        omop_concept_id not in synonyms_language_concept_id
                    ):  # pas de synonyms pour une language
                        for relative_id in relative_ids:
                            # print(relative_id, omop_concept_id)
                            relative_synonym = SynonymModel.find_synonym(
                                concept_id=relative_id,
                                language_concept_id=omop_concept_id,
                                m_algo_id=[
                                    Algo.AUTOMATIC_TRANS.value,
                                    Algo.FROM_OTHER_PROJECTS_TRANS.value,
                                ],
                            )

                            if (
                                relative_synonym
                            ):  # Pas besoin de check les collisions (ie si le synonym existe deja) car ligne # pas de synonym pour le language
                                print(
                                    "synonyms: ",
                                    relative_synonym.concept_id,
                                    relative_synonym.concept_synonym_name,
                                    omop_concept_id,
                                    relative_synonym.m_user_id,
                                )
                                new_synonym = SynonymModel(
                                    concept_id=_id,
                                    concept_synonym_name=relative_synonym.concept_synonym_name,
                                    language_concept_id=omop_concept_id,
                                    m_algo_id=Algo.FROM_OTHER_PROJECTS_TRANS.value,
                                    m_user_id=relative_synonym.m_user_id,
                                )
                                new_synonym.upserting()
                                break

                JobModel(_id, "SYNONYM_FROM_OTHER_PROJECTS_SPARK", 0).upserting()

            JobModel.update_mapper_job(
                dest_status=2, _id=_id, job_type=["EXTEND_SYNONYMS_WITH_OTHER_PROJECT"]
            )


def sync_synonyms_from_vocabulary_id(vocabulary_id_list, project_id_list):
    """
    bulk synonyms transfert
    provide a vocabulary without prefix ex : ccam, project = 1,2 ...
    all the vocabulary : ex APHP - ccam, SNDS - ccam ...
    will be align for synonyms
    Remark : job table only use to synchronise solr!!
    """
    list_with_prefix_voc = []
    if len(vocabulary_id_list) and len(project_id_list):

        for voc in vocabulary_id_list:
            project_name_list = [
                ProjectModel.find_by_id(p).m_project_type_id for p in project_id_list
            ]
            voc_list = [project + SEPARATOR + voc for project in project_name_list]

            # print('voc_list', voc_list)

            concepts_id = []
            for v in voc_list:
                concepts_id += [c.concept_id for c in ConceptModel.find_by_voc(v)]
            nb_concept_id = len(concepts_id)
            # print('len :', nb_concept_id)

            i = 0
            j = 10

            while i < nb_concept_id:
                bash_concepts_id = concepts_id[i:j]
                for id in bash_concepts_id:
                    newJob = JobModel(id, "EXTEND_SYNONYMS_WITH_OTHER_PROJECT", 0)
                    newJob.upserting()
                sync_synonyms_between_projects()
                i += 10
                j += 10
