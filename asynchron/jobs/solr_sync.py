import logging
from pathlib import Path

from pysolrwrapper.loader_postgres.postgres_utils import PostgresClient, PostgresConf
from pysolrwrapper.loader_solr.pg_to_solr import pg_to_solr_query
from pysolrwrapper.loader_solr.solr_utils import SolrClient, SolrConf


class PostgresToSolr:
    def __init__(
        self,
        tmp_csv_path,
        postgres_conf: PostgresConf,
        solr_conf: SolrConf,
        from_scratch: bool,
        debug: bool = False,
    ):
        self.tmp_csv_path = tmp_csv_path
        self.postgres_conf = postgres_conf
        self.solr_conf = solr_conf
        self.from_scratch = from_scratch
        self.debug = debug
        self.solr_client = SolrClient(conf=self.solr_conf)
        self.postgres_client = PostgresClient.from_conf(postgres_conf=self.postgres_conf)

    def _postgres_to_csv(self):
        self.pg_query = pg_to_solr_query(from_scratch=self.from_scratch)
        self.postgres_client.bulk_export_solr(self.pg_query, self.tmp_csv_path)

    def _csv_to_solr(self):
        self.solr_client.solr_bulk_load(self.solr_conf.core, self.tmp_csv_path)
        if not self.debug:
            Path(self.tmp_csv_path).unlink()

    def run(self):
        try:
            self._postgres_to_csv()
            self._csv_to_solr()
        except Exception as e:
            logging.error(self.pg_query)
            logging.error(e)
