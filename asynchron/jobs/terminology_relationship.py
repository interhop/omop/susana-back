import re

from asynchron.models.concept_relationship import ConceptRelationModel
from asynchron.models.project import ProjectModel
from asynchron.models.vocabulary import VocabularyModel
from share.macros import SEPARATOR


def terminology_relationship(list_vocabulary_to_sync=None):
    """
    list_vocabulary_to_sync ['ccam', 'cim10']
    insert links (maps to, mapped from) beetween voc ex 'APHP - ccam' and 'TLS - ccam'
    if does not exist
    """

    distinct_vocabularies = VocabularyModel.find_distinct_vocabulary_id()
    if list_vocabulary_to_sync != None:
        projects = ProjectModel.find_all()
        vocabularies_to_link = []
        for to_sync in list_vocabulary_to_sync:
            vocabularies_to_link += [
                p.m_project_type_id.lower() + SEPARATOR + to_sync.lower()
                for p in projects
            ]
        distinct_vocabularies = [
            v
            for v in distinct_vocabularies
            if v.vocabulary_id.lower() in vocabularies_to_link
        ]

    for voc in distinct_vocabularies:
        project = ProjectModel.find_by_id(voc.m_project_id)
        regex = "^" + project.m_project_type_id + SEPARATOR
        voc_to_test = re.sub(regex, "", voc.vocabulary_id)

        distinct_vocabularies_to_test = [
            v for v in distinct_vocabularies if v.m_project_id != voc.m_project_id
        ]

        for vocabulary_to_test in distinct_vocabularies_to_test:
            project = ProjectModel.find_by_id(vocabulary_to_test.m_project_id)
            regex = "^" + project.m_project_type_id + SEPARATOR

            if (
                voc_to_test.lower()
                == re.sub(regex, "", vocabulary_to_test.vocabulary_id).lower()
            ):

                print(
                    "voc = ",
                    voc.vocabulary_id,
                    " - ",
                    vocabulary_to_test.vocabulary_id,
                    " // ",
                    end="",
                )
                print(voc.vocabulary_concept_id, vocabulary_to_test.vocabulary_concept_id)

                if not ConceptRelationModel.find_by_id(
                    voc.vocabulary_concept_id,
                    vocabulary_to_test.vocabulary_concept_id,
                    "Maps to",
                ):
                    relation = ConceptRelationModel(
                        concept_id_1=voc.vocabulary_concept_id,
                        concept_id_2=vocabulary_to_test.vocabulary_concept_id,
                        relationship_id="Maps to",
                        m_user_id=0,
                        m_algo_id=9,
                    )
                    relation.upserting()

                if not ConceptRelationModel.find_by_id(
                    vocabulary_to_test.vocabulary_concept_id,
                    voc.vocabulary_concept_id,
                    "Mapped from",
                ):
                    reverse_relation = ConceptRelationModel(
                        concept_id_2=voc.vocabulary_concept_id,
                        concept_id_1=vocabulary_to_test.vocabulary_concept_id,
                        relationship_id="Mapped from",
                        m_user_id=0,
                        m_algo_id=9,
                    )
                    reverse_relation.upserting()
