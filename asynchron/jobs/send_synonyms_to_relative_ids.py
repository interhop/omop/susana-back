from asynchron.models.concept import ConceptModel
from asynchron.models.job import JobModel
from asynchron.models.project import ProjectModel
from asynchron.models.synonym import SynonymModel
from share.macros import ISO_OMOP_LANGUAGES, SEPARATOR, VOCABULARY_TO_LINK, Algo


# synonyms sender (for one id)
def extend_synonyms_for_one_concept():
    # take synonymfrom translate API or from_other_project_trans
    # base from on concept_id
    # => search relative_concept_id ie same vocabulary_id ('APHP - ccam' and 'LILLE - ccam') and same concept_code
    # => check if synonyms for the concept
    # => catch synonym for this concept_id

    result = JobModel.update_mapper_job(
        initial_status=[0, 1, 3],
        dest_status=1,
        job_type=["EXTEND_SYNONYMS_TO_OTHER_PROJECT"],
    )
    print(len(result))

    if len(result) > 0:
        for _id, m_user_id in result:

            print("sourceconcept: ", _id, ConceptModel.find_by_id(_id).concept_name)

            # find concept_id with JOIN on voc and concept_code
            relative_concepts = ConceptModel.find_concept_in_similar_voc(
                _id
            )  # return list
            relative_ids = [c.concept_id for c in relative_concepts]

            if len(relative_ids) > 0:

                # get all the synonym for the concept_id tested (API_translate + if other_project)
                source_synonyms = SynonymModel.find_synonym(
                    _id,
                    m_algo_id=[
                        Algo.AUTOMATIC_TRANS.value,
                        Algo.FROM_OTHER_PROJECTS_TRANS.value,
                    ],
                )
                source_synonyms_language_concept_id = [
                    s.language_concept_id for s in source_synonyms
                ]
                print(
                    "source_synonyms_language_concept_id",
                    source_synonyms_language_concept_id,
                )

                if len(source_synonyms_language_concept_id) > 0:

                    for relative_id in relative_ids:
                        print("new synonyms: ", relative_id)

                        relative_synonyms = SynonymModel.find_synonym(
                            relative_id,
                            m_algo_id=[
                                Algo.AUTOMATIC_TRANS.value,
                                Algo.FROM_OTHER_PROJECTS_TRANS.value,
                            ],
                        )
                        relative_synonyms_language_concept_id = [
                            s.language_concept_id for s in relative_synonyms
                        ]
                        print(
                            "relative_synonyms_language_concept_id",
                            relative_synonyms_language_concept_id,
                        )

                        for _, omop_concept_id in ISO_OMOP_LANGUAGES.items():

                            if (
                                omop_concept_id
                                not in relative_synonyms_language_concept_id
                                and omop_concept_id in source_synonyms_language_concept_id
                            ):  # pas de synonyms pour une language

                                source_synonym = source_synonyms[
                                    source_synonyms_language_concept_id.index(
                                        omop_concept_id
                                    )
                                ]
                                print(
                                    "new synonyms: ",
                                    relative_id,
                                    source_synonym.concept_synonym_name,
                                    omop_concept_id,
                                    source_synonym.m_user_id,
                                )
                                new_synonym = SynonymModel(
                                    concept_id=relative_id,
                                    concept_synonym_name=source_synonym.concept_synonym_name,
                                    language_concept_id=omop_concept_id,
                                    m_algo_id=Algo.FROM_OTHER_PROJECTS_TRANS.value,
                                    m_user_id=source_synonym.m_user_id,
                                )
                                new_synonym.upserting()

                JobModel(_id, "SYNONYM_TO_OTHER_PROJECTS_SPARK", 0).upserting()

            JobModel.update_mapper_job(
                dest_status=2, _id=_id, job_type=["EXTEND_SYNONYMS_TO_OTHER_PROJECT"]
            )


def synonyms_to_other_projects(vocabulary_ids_list=None, project_ids_list=None):
    """
    bulk synonyms to transfert
    provide vocabylary without prefix ex : ['ccam'] +/- project from which you want to export [4]
    all the vocabulary will be extend with prefix : ex 'APHP - ccam', 'SNDS - ccam' ...

    Remark : job table only use to synchronise solr!!
    """

    if not project_ids_list:
        project_ids_list = [p.m_project_id for p in ProjectModel.find_all()]

    if not vocabulary_ids_list:
        vocabulary_ids_list = VOCABULARY_TO_LINK

    for voc in vocabulary_ids_list:
        project_name_list = [
            ProjectModel.find_by_id(p).m_project_type_id for p in project_ids_list
        ]
        voc_list = [project + SEPARATOR + voc for project in project_name_list]

        for v in voc_list:
            concepts_id = []
            for v in voc_list:
                concepts_id += [c.concept_id for c in ConceptModel.find_by_voc(v)]
            nb_concept_id = len(concepts_id)
            # print('len :', nb_concept_id)

            i = 0
            j = 10

            while i < nb_concept_id:
                bash_concepts_id = concepts_id[i:j]
                for id in bash_concepts_id:
                    newJob = JobModel(id, "EXTEND_SYNONYMS_TO_OTHER_PROJECT", 0)
                    newJob.upserting()
                extend_synonyms_for_one_concept()
                i += 10
                j += 10
